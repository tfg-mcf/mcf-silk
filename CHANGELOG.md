# Application Changelog

## [Version `0.0.4`](https://gitlab.com/tfg-mcf/mcf-silk/tags/v0.0.4)

> "_Application patch version 0.0.4_"

### Overview

This release includes a fully usable set of product entity model classes (except a media gallery add-on. This will come in a future release, hopefully 0.0.5).

### Release Notes

* Added new utility classes for Magento product statuses - `Mage\Api\Entities\Catalog\Product\ProductEntityStatus`
* Added new utility classes for Magento product visibilities - `Mage\Api\Entities\Catalog\Product\ProductEntityVisibility`
* Added new utility classes for Magento product types - `Mage\Api\Entities\Catalog\Product\ProductEntityType`
* Added product tier prices entity model - `Mage\Api\Entities\Catalog\Product\TierPricesEntity`
* Added product custom attribute entity model - `Mage\Api\Entities\Framework\AttributeEntity`

#### Deprecations

* Class: `Mage\Product\Product`
  * Replaced by: `Mage\Api\Entites\Catalog\Product\ProductEntity`

---

## [Version `0.0.3`](https://gitlab.com/tfg-mcf/mcf-silk/tags/v0.0.3)

> "_Upgrade application verson to 0.0.3_"

### Overview

This release aims to improve database interactions, along with implementing better tests for databases. A large emphasis was put on writing tests and using coverage tags to ensure we are covering our code as best as possible.

### Release Notes

#### Architecture Changes

* Integrated `symfony/options-resolver 3.x` component to aid in the configuration of class options & option validation.
* Add DBUnit to supplement PHPUnit for testing databases.
* Implemented the repository pattern within the `Mage\Api` namespace for managing Magento 2 API endpoint usage.
* Add a database connection client for `Silk` namespace.
* Add a database driver resolver helper class for `Silk\Database\Client`
* Add API documentation using phpDocumentor `2.9`.

---

## [Version `0.0.2`](https://gitlab.com/tfg-mcf/mcf-silk/tags/v0.0.2)

> "_Patch version 0.0.2_"

### Overview

This release aims to cleanup (or at least start to) a lot of the technical debt that was built up duing development of the first prototype release. This is done by reworking the directory structure for a smoother transition to PSR-4 autoloading in a future release, adding DI/IoC, and preparing for the implementation of the repository pattern among other things.  

### Release Notes

#### Architecture Changes

* Reworked application directory structure to better suite the standard structure supported by larger projects like Symfony/Laravel.
  * This will make the transition from a PSR-0 autoloading interface to a PSR-4 autoloading interface much smoother.
* Added support for Cronitor cron job monitoring service. Using the `jorb/cronitor-php 0.1.1` package.
* Added a wrapper around monolog for tigher control over logger dependencies & logfile channels/levels.
* Integrated `symfony/dotenv 3.x` component for environment variable handling.
* Integrated `symfony/config 3.x` component for handling configuration values and verification.
* Integrated `symfony/dependency-injection 3.x` component for handling DI/IoC of the increasing amount of packages/components that are being used.
* Integrated `symfony/filesystem 3.x` component to help with filesystem operations (finding, resolving, and verifying config/service registration files).
* Integrated `symfony/finder 3.x` component to supplement the filesystem component (finding, resolving, and verifying config/service registration files).
* Renamed command `product:update:qty` to `catalog:run:inventory`.
* Renamed command `product:update:price` to `catalog:run:pricing`.
* Renamed command `product:update:json` to `product:update:via`. The command now detects the file type and serializers it accordingly for use as a PHP object.
* Wrapped command `import:product` in the new Symfony console command component.
* Moved the HTTP client from the `Silk\Api` namespace to the new `Mage\Api` namespace.
* Moved the product entity from the `Silk\Product` namespace to the new `Mage\Product` namespace.

#### Deprecations

* All uses of the `Silk\Utils::silk_math()` method has been deprecated in favour of the new `Silk\Math` class and it's methods `::multiply` & `::divide`.
  * Combine the use of `Silk\Math` with the new `Silk\UnitOfMeasure` static class.
* All uses of the `Mage\Api\Client::addProduct()` method has been deprecated in preperation for the Repository pattern being introduced in the next release.
* All uses of the `Mage\Api\Client::updateProduct()` method has been deprecated in preperation for the Repository pattern being introduced in the next release.
* All uses of the `Mage\Api\Client::addAttributeOption()` method has been deprecated in preperation for the Repository pattern being introduced in the next release.
* All uses of the `Mage\Api\Client::kill()` method has been deprecated in preperation for the Repository pattern being introduced in the next release.

#### New Commands

* Add command `attribute:add:option`. Add an option to an existing product attribute.
* Add command `configuration:dump`. Print the current application configuration.
* Add command `dev:tests:run`. Run the application test suite from the application itself.
* Add command `service:dump`. Print the current set of registered application services.

#### Removed Commands

Several commands have been removed, either due to the high cost of maintenance or due to being functionally replaced by `product:update:via`.

* Removed `crawl:product:name`.
* Removed `crawl:product-description:short`.
* Removed `crawl:product-description:long`.
* Removed `product:update:name:manual`.
* Removed `scrape:product:name`.
* Removed `scrape:product-description:short`.
* Removed `scrape:product-description:long`.
* Removed `vendor:assets:sort`.

---

## [Version `0.0.1`](https://gitlab.com/tfg-mcf/mcf-silk/tags/v0.0.1)

> "_Working prototype release_"

### Overview

This release aims to improve the application that bridges Anzio Silk to an instance of Magento 2. Primarily by turning the few PHP bash scripts & SQL query files into a full fledged console application.

### Release Notes

#### Architecture Changes

* Add composer support with PSR-0 autoloading.
* Application was moved from a set of PHP bash scripts to using the Symfony framework console component (`symfony/console 3.x`)
* Wrapped command `product:update:price` in the new Symfony console command component.
* Wrapped command `product:update:qty` in the new Symfony console command component.
* Wrapped command `product:add:image` in the new Synfony console command component.
* Wrapped command `product:update:attribute-set`. in the new Synfony console command component.

#### New Commands

A set of "smart" `crawl` commands has been added to help cultivate product data from vendors that do not provide a spreadsheet of data for us.

* Add command `crawl:product:name`. Used to crawl for a product name on a vendors' website.
* Add command `crawl:product-description:short`. Used to crawl for a product short description on a vendors' website.
* Add command `crawl:product-description:long`. Used to crawl for a product long description on a vendors' website.

Product update commands have been added to aid in the normalization of product data. Manually normalizing products via Magento 2 admin GUI is not feasable.

* Add command `product:update:json`. Update products through a json file.
* Add command `product:update:categories`. Update a products categories.
* Add command `product:update:name:manual`. Change the names of one or more products.

Along with the `crawl` commands, a set of "dumb" scrapers are available to use for vendor websites that do not use a proper URL schema for efficient crawling.

* Add command `scrape:product:name`. Use this command to scrape the web and change the names of your products.
* Add command `scrape:product-description:short`. Use this command to scrape the web and change the short description of your products.
* Add command `scrape:product-description:long`. Use this command to scrape the web and change the long description of your products.

Other misc. commands that were added;

* Add command `vendor:assets:sort`. Used to sort large directories of images into explicit SKU directories.
