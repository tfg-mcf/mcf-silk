<?php

declare(strict_types = 1);

namespace Silk\Tests\Database\Entities\Inventory\Product;

use PHPUnit\Framework\TestCase;
use Silk\Database\Entities\Inventory\Product\Invf12;

/**
* @coversNothing
*/
class Invf12Spec extends TestCase
{
  protected function setUp ()
  {
    $this->entityVersion = "8.3b";
    $this->invf12 = new Invf12;
  }

  protected function tearDown ()
  {
    unset($this->invf12);
    unset($this->entityVersion);
  }

  public function test_entity_version ()
  {
    $mirror = new \ReflectionClass(Invf12::class);
    $this->assertArrayHasKey("VERSION", $mirror->getConstants());
    $this->assertInternalType("string", $this->invf12::VERSION);
    $this->assertEquals($this->entityVersion, $this->invf12::VERSION);
  }

  public function test_instance_attributes ()
  {
    $this->assertInstanceOf(Invf12::class, $this->invf12);
    $this->assertClassHasAttribute("itemcode", Invf12::class);
  }
}
