<?php

declare(strict_types = 1);

namespace Silk\Tests\Database\Entities\Inventory\Product;

use PHPUnit\Framework\TestCase;
use Silk\Database\Entities\Inventory\Product\Invf02;

/**
* @coversNothing
*/
class Invf02Spec extends TestCase
{
  protected function setUp ()
  {
    $this->entityVersion = "8.3b";
    $this->invf02 = new Invf02;
  }

  protected function tearDown ()
  {
    unset($this->invf02);
    unset($this->entityVersion);
  }

  public function test_entity_version ()
  {
    $mirror = new \ReflectionClass(Invf02::class);
    $this->assertArrayHasKey("VERSION", $mirror->getConstants());
    $this->assertInternalType("string", $this->invf02::VERSION);
    $this->assertEquals($this->entityVersion, $this->invf02::VERSION);
  }

  public function test_instance_attributes_exist ()
  {
    $this->assertInstanceOf(Invf02::class, $this->invf02);
    $this->assertClassHasAttribute("itemcode", Invf02::class);
  }
}
