<?php

declare (strict_types = 1);

namespace Silk\Tests\Database\Drivers;

use PDO;
use PHPUnit\Framework\TestCase;
use Silk\Database\Client;
use Silk\Database\Drivers\DriverResolver;

/**
 * @coversDefaultClass Silk\Database\Drivers\DriverResolver
 * @uses Silk\Database\Drivers\DriverResolver
 */
class DriverResolverSpec extends TestCase
{
  protected function setUp ()
  {
    $this->resolver = new DriverResolver(new Client([
      "host" => "mysql",
      "schema" => "test_silk",
    ]));
  }

  protected function tearDown ()
  {
    unset($this->resolver);
  }

  /**
   * @covers ::resolve
   * @uses Silk\Database\Client
   */
  public function test_resolver_returns_PDO_instance ()
  {
    $driver = $this->resolver->resolve();

    $this->assertInstanceOf(PDO::class, $driver);
  }
}
