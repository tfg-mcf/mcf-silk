<?php

declare (strict_types = 1);

namespace Silk\Tests\Database;

use PHPUnit\Framework\TestCase;
use Silk\Database\Client;

/**
 * @coversDefaultClass Silk\Database\Client
 * @uses Silk\Database\Client
 */
class ClientSpec extends TestCase
{
  protected function setUp ()
  {
    $this->client = new Client([
      "host" => "mysql",
      "schema" => "test_silk",
    ]);
  }

  protected function tearDown ()
  {
    unset($this->client);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::disconnect
   */
  public function test_database_method_disconnect ()
  {
    $this->client->connect();
    $this->client->disconnect();

    $expected = null;
    $actual = $this->client->getConnectionStatus();

    $this->assertEquals($expected, $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::connect
   */
  public function test_database_method_connect ()
  {
    $this->client->disconnect();
    $this->client->connect();

    $expected = "mysql via TCP/IP";
    $actual = $this->client->getConnectionStatus();

    $this->assertEquals($expected, $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getConnectionStatus
   */
  public function test_local_connection_status ()
  {
    $expected = "mysql via TCP/IP";
    $actual = $this->client->getConnectionStatus();

    $this->assertEquals($expected, $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getDriver
   */
  public function test_database_method_getDriver ()
  {
    $expected = "mysql";
    $actual = $this->client->getDriver();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getHost
   */
  public function test_database_method_getHost ()
  {
    $expected = "mysql";
    $actual = $this->client->getHost();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getPort
   */
  public function test_database_method_getPort ()
  {
    $expected = 3306;
    $actual = $this->client->getPort();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("int", $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getSchema
   */
  public function test_database_method_getSchema ()
  {
    $expected = "test_silk";
    $actual = $this->client->getSchema();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getDSN
   */
  public function test_database_method_getDSN ()
  {
    $expected = "mysql:host=mysql;port=3306;dbname=test_silk";
    $actual = $this->client->getDSN();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getUsername
   */
  public function test_database_method_getUsername ()
  {
    $expected = "root";
    $actual = $this->client->getUsername();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @uses Silk\Database\Drivers\DriverResolver
   * @covers ::getPassword
   */
  public function test_database_method_getPassword ()
  {
    $expected = "root";
    $actual = $this->client->getPassword();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }
}
