<?php

declare (strict_types = 1);

namespace Silk\Tests\Database;

use PDO;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

class QuerySpec extends TestCase
{
  use TestCaseTrait;

  protected function getConnection ()
  {
    $this->client = new PDO(
      "mysql:host=mysql;port=3306;dbname=test_silk;",
      "root",
      "root"
    );
    $this->client->query("CREATE TABLE invf12 (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      itemcode VARCHAR(15) NOT NULL,
      quantityreserved FLOAT(6) NOT NULL,
      quantityonhand FLOAT(6) NOT NULL,
      qtyonorder FLOAT(6) NOT NULL,
      levelcprice1 FLOAT(6) NOT NULL
    );");
    return $this->createDefaultDBConnection($this->client, 'test_silk');
  }

  protected function getDataSet ()
  {
    return new ArrayDataSet([
      "invf12" => [
        [
          "id" => 23,
          "itemcode" => "MIT2100-20",
          "quantityreserved" => 0.00,
          "quantityonhand" => 0.50,
          "qtyonorder" => 0.25,
          "levelcprice1" => 2.50,
        ],
        [
          "id" => 24,
          "itemcode" => "MKTDST157Z",
          "quantityreserved" => 0.00,
          "quantityonhand" => 0.50,
          "qtyonorder" => 0.25,
          "levelcprice1" => 2.50,
        ],
        [
          "id" => 25,
          "itemcode" => "3MA123",
          "quantityreserved" => 0,
          "quantityonhand" => 100,
          "qtyonorder" => 0,
          "levelcprice1" => 1.25,
        ]
      ]
    ]);
  }

  /**
   * @coversNothing
   */
  public function test_row_count()
  {
    $db = $this->getConnection();

    $this->assertEquals(3, $db->getRowCount("invf12"));
  }
}
