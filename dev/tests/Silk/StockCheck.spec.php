<?php

namespace Silk\Tests;

use PHPUnit\Framework\TestCase;
use Silk\Utils;

class StockCheckSpec extends TestCase
{
  /**
   * @covers Silk\Utils::is_in_stock
   */
  public function test_is_in_stock_returns_correct_boolean ()
  {
    $this->assertTrue(Utils::is_in_stock(1));
    $this->assertTrue(Utils::is_in_stock(2.328E-8));
    $this->assertFalse(Utils::is_in_stock(0));
    $this->assertFalse(Utils::is_in_stock((int) 2.328E-8));
    $this->assertFalse(Utils::is_in_stock(-1));
  }
}
