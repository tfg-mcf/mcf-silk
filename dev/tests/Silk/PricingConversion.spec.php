<?php

namespace Silk\Tests;

use PHPUnit\Framework\TestCase;
use Silk\{ Math, UnitOfMeasure };

class PricingConversionSpec extends TestCase
{
  /**
   * @covers Silk\Math::divide
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_pricing_values_per_ea ()
  {
    $price = 35.99;
    $unit = 'EA'; // per each

    $this->assertEquals(35.99, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.599, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.3599, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3599, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(35990, Math::divide($price, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::divide
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_pricing_values_per_bx ()
  {
    $price = 451.94;
    $unit = 'BX'; // per box

    $this->assertEquals(451.94, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(45194, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(451940, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.45194, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(4.5194, Math::divide($price, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::divide
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_pricing_values_per_cs ()
  {
    $price = 120.30;
    $unit = 'CS'; // per case

    $this->assertEquals(120.30, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(1.2030, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.12030, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(12030, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(120300, Math::divide($price, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::divide
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_pricing_values_per_pr ()
  {
    $price = 4194.90;
    $unit = 'PR'; // per pair

    $this->assertEquals(4194.90, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(419490, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(4194900, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.419490, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(4.19490, Math::divide($price, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::divide
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_pricing_values_per_100 ()
  {
    $price = 6.36;
    $unit = '/C'; // per 100

    $this->assertEquals(0.06, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.60, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(6.36, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(636, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(6360, Math::divide($price, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::divide
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_pricing_values_per_1000 ()
  {
    $price = 18.70;
    $unit = '/M'; // per 1,000

    $this->assertEquals(0.02, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(18.70, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.20, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(1870, Math::divide($price, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(18700, Math::divide($price, UnitOfMeasure::getValue($unit)));
  }
}
