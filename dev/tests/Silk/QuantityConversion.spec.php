<?php

namespace Silk\Tests;

use PHPUnit\Framework\TestCase;
use Silk\{ Math, UnitOfMeasure };

class StockConversionSpec extends TestCase
{
  /**
   * @covers Silk\Math::multiply
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_quantity_values_per_ea ()
  {
    $qty = 35;
    $unit = 'EA'; // per each

    $this->assertEquals(35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.5, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(350, Math::multiply($qty, UnitOfMeasure::getValue($unit)));

    $this->assertEquals(0, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertEquals((int) 2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::multiply
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_quantity_values_per_bx ()
  {
    $qty = 35;
    $unit = 'BX'; // per box

    $this->assertEquals(35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.5, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(350, Math::multiply($qty, UnitOfMeasure::getValue($unit)));

    $this->assertEquals(0, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertEquals((int) 2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::multiply
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_quantity_values_per_cs ()
  {
    $qty = 35;
    $unit = 'CS'; // per case

    $this->assertEquals(35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.5, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(350, Math::multiply($qty, UnitOfMeasure::getValue($unit)));

    $this->assertEquals(0, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertEquals((int) 2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::multiply
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_quantity_values_per_pr ()
  {
    $qty = 35;
    $unit = 'PR'; // per pair

    $this->assertEquals(35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.5, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(0.35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(350, Math::multiply($qty, UnitOfMeasure::getValue($unit)));

    $this->assertEquals(0, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertEquals((int) 2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::multiply
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_quantity_values_per_100 ()
  {
    $qty = 0.35;
    $unit = '/C'; // per 100

    $this->assertEquals(35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.5, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(350, Math::multiply($qty, UnitOfMeasure::getValue($unit)));

    $this->assertEquals(0, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertEquals((int) 2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
  }

  /**
   * @covers Silk\Math::multiply
   * @uses Silk\UnitOfMeasure
   */
  public function test_silk_math_correctly_calculates_human_readable_quantity_values_per_1000 ()
  {
    $qty = 0.35;
    $unit = '/M'; // per 1,000

    $this->assertEquals(350, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(3.5, Math::multiply($qty, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(35, Math::multiply($qty, UnitOfMeasure::getValue($unit)));

    $this->assertEquals(0, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertEquals((int) 2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
    $this->assertNotEquals(2.328E-8, Math::multiply(0.00, UnitOfMeasure::getValue($unit)));
  }
}
