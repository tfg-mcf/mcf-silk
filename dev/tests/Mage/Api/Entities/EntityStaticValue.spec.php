<?php

declare(strict_types = 1);

namespace Mage\Tests\Api\Entities;

use Mage\Api\Entities\EntityStaticValue;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass Mage\Api\Entities\EntityStaticValue
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class EntityStaticValueSpec extends TestCase
{
  /**
   * @covers ::getStaticValue
   */
  public function test_method_getStaticValue_returns_value ()
  {
    $expected = "One";
    $actual = EntityStaticValue::getStaticValue(1, [
      1 => "One",
      2 => "Two",
      3 => "Three"
    ]);

    $this->assertEquals($expected, $actual);
  }
}
