<?php

declare(strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\ProductEntityStatus;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass Mage\Api\Entities\Catalog\Product\ProductEntityStatus
 * @uses Mage\Api\Entities\EntityStaticValue
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class ProductEntityStatusSpec extends TestCase
{
  /**
   * @covers ::getStatusValue
   */
  public function test_status_enabled ()
  {
    $expectedId = 1;
    $actualId = ProductEntityStatus::ENABLED;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Enabled";
    $actualLabel = ProductEntityStatus::getStatusValue(1);
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getStatusValue
   */
  public function test_status_disabled ()
  {
    $expectedId = 2;
    $actualId = ProductEntityStatus::DISABLED;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Disabled";
    $actualLabel = ProductEntityStatus::getStatusValue(2);
    $this->assertEquals($expectedLabel, $actualLabel);
  }
}
