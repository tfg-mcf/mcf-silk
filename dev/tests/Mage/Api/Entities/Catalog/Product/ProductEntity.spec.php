<?php

declare (strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\ProductEntity;
use Mage\Api\Entities\Catalog\Product\ExtensionEntity;
use Mage\Api\Entities\Catalog\Product\TierPricesEntity;
use Mage\Api\Entities\Catalog\Inventory\StockItemEntity;
use Mage\Api\Entities\Catalog\Product\{
  ProductEntityStatus as Status,
  ProductEntityType as Type,
  ProductEntityVisibility as Visibility
};
use Mage\Api\Entities\Framework\AttributeEntity;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Test the Magento 2 product entity model class adheres to the specs.
 *
 * @coversDefaultClass Mage\Api\Entities\Catalog\Product\ProductEntity
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class ProductEntitySpec extends TestCase
{
  protected function setUp ()
  {
    $product = new ProductEntity;
    $this->product = $product;

    $this->serializer = new Serializer(
      array(
        new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter)
      ),
      array(
        new JsonEncoder
      )
    );
  }

  protected function tearDown ()
  {
    unset($this->product);
    unset($this->serializer);
  }

  /**
   * @coversNothing
   */
  public function test_product_construction_json_serialization ()
  {
    $stockItem = new StockItemEntity;
    $stockItem
      ->setQty(10.5)
      ;

    $extension = new ExtensionEntity;
    $extension
      ->setStockItem($stockItem)
      ;

    $tierA = new TierPricesEntity;
    $tierB = new TierPricesEntity;
    $tierPrices = array($tierA, $tierB);
    $tierA
      ->setCustomerGroupId(1)
      ->setQty(1)
      ->setValue(0.90)
      ;
    $tierB
      ->setCustomerGroupId(2)
      ->setQty(1)
      ->setValue(0.80)
      ;

    $customAttributeA = new AttributeEntity;
    $customAttributeB = new AttributeEntity;
    $customAttributes = array($customAttributeA, $customAttributeB);
    $customAttributeA
      ->setAttributeCode("custom_attr_a")
      ->setValue("1-1/4\"")
      ;
    $customAttributeB
      ->setAttributeCode("custom_attr_b")
      ->setValue("1-1/2\"")
      ;

    $this->product
      ->setId(123)
      ->setSku("SKU")
      ->setName("Product")
      ->setAttributeSetId(1)
      ->setPrice(1.00)
      ->setStatus(Status::DISABLED)
      ->setVisibility(Visibility::IS_VISIBLE)
      ->setTypeId(Type::SIMPLE)
      ->setCreatedAt(date('Y-m-d H:i:s'))
      ->setUpdatedAt(date('Y-m-d H:i:s'))
      ->setWeight(13.3)
      ->setExtensionAttributes($extension)
      ->setTierPrices($tierPrices)
      ->setCustomAttributes($customAttributes)
      ;

    $actual = $this->serializer->serialize($this->product, "json");
    $expected = json_encode([
      "id" => 123,
      "sku" => "SKU",
      "name" => "Product",
      "attribute_set_id" => 1,
      "price" => 1.00,
      "status" => 2,
      "visibility" => 4,
      "type_id" => "simple",
      "created_at" => date('Y-m-d H:i:s'),
      "updated_at" => date('Y-m-d H:i:s'),
      "weight" => 13.3,
      "extension_attributes" => [
        "stock_item" => [
          "qty" => 10.5,
          "in_stock" => true,
        ],
      ],
      "tier_prices" => [
        [
          "customer_group_id" => 1,
          "qty" => 1,
          "value" => 0.90
        ],
        [
          "customer_group_id" => 2,
          "qty" => 1,
          "value" => 0.80
        ],
      ],
      "custom_attributes" => [
        [
          "attribute_code" => "custom_attr_a",
          "value" => "1-1/4\"",
        ],
        [
          "attribute_code" => "custom_attr_b",
          "value" => "1-1/2\"",
        ],
      ],
    ]);

    $this->assertJsonStringEqualsJsonString($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @covers ::setId
   * @covers ::getId
   */
  public function test_methods_setId_getId ()
  {
    $expected = 19373;
    $this->product->setId($expected);
    $actual = $this->product->getId();

    $this->assertInternalType("int", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setSku
   * @covers ::getSku
   */
  public function test_methods_setSku_getSku ()
  {
    $expected = "SKU123";
    $this->product->setSku($expected);
    $actual = $this->product->getSku();

    $this->assertInternalType("string", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setName
   * @covers ::getName
   */
  public function test_methods_setName_getName ()
  {
    $expected = "Unit Test Product Name";
    $this->product->setName($expected);
    $actual = $this->product->getName();

    $this->assertInternalType("string", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setAttributeSetId
   * @covers ::getAttributeSetId
   */
  public function test_methods_setAttributeSetId_getAttributeSetId ()
  {
    $expected = 1;
    $this->product->setAttributeSetId($expected);
    $actual = $this->product->getAttributeSetId();

    $this->assertInternalType("int", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setPrice
   * @covers ::getPrice
   */
  public function test_methods_setPrice_getPrice ()
  {
    $expected = 10.50;
    $this->product->setPrice($expected);
    $actual = $this->product->getPrice();

    $this->assertInternalType("float", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setStatus
   * @covers ::getStatus
   * @uses Mage\Api\Entities\Catalog\Product\ProductEntityStatus
   */
  public function test_methods_setStatus_getStatus ()
  {
    $expected = Status::ENABLED;
    $this->product->setStatus($expected);
    $actual = $this->product->getStatus();

    $this->assertInternalType("int", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setVisibility
   * @covers ::getVisibility
   * @uses Mage\Api\Entities\Catalog\Product\ProductEntityVisibility
   */
  public function test_methods_setVisibility_getVisibility ()
  {
    $expected = Visibility::IS_VISIBLE;
    $this->product->setVisibility($expected);
    $actual = $this->product->getVisibility();

    $this->assertInternalType("int", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setTypeId
   * @covers ::getTypeId
   */
  public function test_methods_setTypeId_getTypeId ()
  {
    $expected = Type::SIMPLE;
    $this->product->setTypeId($expected);
    $actual = $this->product->getTypeId();

    $this->assertInternalType("string", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setCreatedAt
   * @covers ::getCreatedAt
   */
  public function test_methods_setCreatedAt_getCreatedAt ()
  {
    $expected = "2017-05-26 17:00:21";
    $this->product->setCreatedAt($expected);
    $actual = $this->product->getCreatedAt();

    $this->assertInternalType("string", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setUpdatedAt
   * @covers ::getUpdatedAt
   */
  public function test_methods_setUpdatedAt_getUpdatedAt ()
  {
    $expected = "2017-06-05 12:01:54";
    $this->product->setUpdatedAt($expected);
    $actual = $this->product->getUpdatedAt();

    $this->assertInternalType("string", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setWeight
   * @covers ::getWeight
   */
  public function test_methods_setWeight_getWeight ()
  {
    $expected = 12.53;
    $this->product->setWeight($expected);
    $actual = $this->product->getWeight();

    $this->assertInternalType("float", $actual);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::setExtensionAttributes
   * @covers ::getExtensionAttributes
   */
  public function test_methods_setExtensionAttributes_getExtensionAttributes ()
  {
    $actual = new ExtensionEntity;
    $this->product->setExtensionAttributes($actual);
    // $expected = $this->getExtensionAttributes();
    $expected = ExtensionEntity::class;

    $this->assertInstanceOf($expected, $actual);
  }

  /**
   * @covers ::setTierPrices
   * @covers ::getTierPrices
   */
  public function test_methods_setTierPrices_getTierPrices ()
  {
    $tier = array(new TierPricesEntity);
    $this->product->setTierPrices($tier);

    $actual = $this->product->getTierPrices();
    $expected = $tier;

    $this->assertInternalType("array", $actual);
    $this->assertInstanceOf(TierPricesEntity::class, $actual[0]);
  }

  /**
   * @covers ::setCustomAttributes
   * @covers ::getCustomAttributes
   */
  public function test_methods_setCustomAttributes_getCustomAttributes ()
  {
    $attribute = array(new AttributeEntity);
    $this->product->setCustomAttributes($attribute);

    $actual = $this->product->getCustomAttributes();
    $expected = $attribute;

    $this->assertInternalType("array", $actual);
    $this->assertInstanceOf(AttributeEntity::class, $actual[0]);
  }
}
