<?php

declare (strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\TierPricesEntity;
use Mage\Api\Entities\Catalog\Inventory\StockItemEntity;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * @coversDefaultClass Mage\Api\Entities\Catalog\Product\TierPricesEntity
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class TierPricesEntitySpec extends TestCase
{
  protected function setUp ()
  {
    $this->tierPrices = new TierPricesEntity;
    $this->serializer = new Serializer(
      array(
        new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter)
      ),
      array(
        new JsonEncoder
      )
    );
  }

  protected function tearDown ()
  {
    unset($this->tierPrices);
    unset($this->serializer);
  }

  /**
   * @coversNothing
   */
  public function test_tier_prices_construction_json_serialization ()
  {
    $this->tierPrices
      ->setCustomerGroupId(3)
      ->setQty(0.42)
      ->setValue(140.99)
      ;

    $actual = $this->serializer->serialize($this->tierPrices, "json");
    $expected = json_encode([
      "customer_group_id" => 3,
      "qty" => 0.42,
      "value" => 140.99,
    ]);

    $this->assertJsonStringEqualsJsonString($actual, $expected);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @covers ::setCustomerGroupId
   * @covers ::getCustomerGroupId
   */
  public function test_methods_setCustomerGroupId_getCustomerGroupId ()
  {
    $this->tierPrices->setCustomerGroupId(3);

    $expected = 3;
    $actual = $this->tierPrices->getCustomerGroupId();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("int", $actual);
  }

  /**
   * @covers ::setQty
   * @covers ::getQty
   */
  public function test_methods_setQty_getQty ()
  {
    $this->tierPrices->setQty(3.60);

    $expected = 3.60;
    $actual = $this->tierPrices->getQty();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("float", $actual);
  }

  /**
   * @covers ::setValue
   * @covers ::getValue
   */
  public function test_methods_setValue_getValue ()
  {
    $this->tierPrices->setValue(74.62);

    $expected = 74.62;
    $actual = $this->tierPrices->getValue();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("float", $actual);
  }
}
