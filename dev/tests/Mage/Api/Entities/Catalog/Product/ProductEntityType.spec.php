<?php

declare(strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\ProductEntityType;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass Mage\Api\Entities\Catalog\Product\ProductEntityType
 * @uses Mage\Api\Entities\EntityStaticValue
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class ProductEntityTypeSpec extends TestCase
{
  /**
   * @covers ::getTypeValue
   */
  public function test_type_simple ()
  {
    $expectedId = "simple";
    $actualId = ProductEntityType::SIMPLE;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Simple Product";
    $actualLabel = ProductEntityType::getTypeValue("simple");
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getTypeValue
   */
  public function test_type_configurable ()
  {
    $expectedId = "configurable";
    $actualId = ProductEntityType::CONFIGURABLE;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Configurable Product";
    $actualLabel = ProductEntityType::getTypeValue("configurable");
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getTypeValue
   */
  public function test_type_grouped ()
  {
    $expectedId = "grouped";
    $actualId = ProductEntityType::GROUPED;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Grouped Product";
    $actualLabel = ProductEntityType::getTypeValue("grouped");
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getTypeValue
   */
  public function test_type_bundle ()
  {
    $expectedId = "bundle";
    $actualId = ProductEntityType::BUNDLE;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Bundle Product";
    $actualLabel = ProductEntityType::getTypeValue("bundle");
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getTypeValue
   */
  public function test_type_virtual ()
  {
    $expectedId = "virtual";
    $actualId = ProductEntityType::VIRTUAL;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Virtual Product";
    $actualLabel = ProductEntityType::getTypeValue("virtual");
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getTypeValue
   */
  public function test_type_downloadable ()
  {
    $expectedId = "downloadable";
    $actualId = ProductEntityType::DOWNLOADABLE;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Downloadable Product";
    $actualLabel = ProductEntityType::getTypeValue("downloadable");
    $this->assertEquals($expectedLabel, $actualLabel);
  }
}
