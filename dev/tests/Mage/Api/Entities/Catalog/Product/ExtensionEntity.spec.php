<?php

declare (strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\ExtensionEntity;
use Mage\Api\Entities\Catalog\Inventory\StockItemEntity;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * @coversDefaultClass Mage\Api\Entities\Catalog\Product\ExtensionEntity
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class ExtensionEntitySpec extends TestCase
{
  protected function setUp ()
  {
    $this->extension = new ExtensionEntity;
    $this->serializer = new Serializer(
      array(
        new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter)
      ),
      array(
        new JsonEncoder
      )
    );
  }

  protected function tearDown ()
  {
    unset($this->extension);
    unset($this->serializer);
  }

  /**
   * @coversNothing
   */
  public function test_extension_construction_json_serialization ()
  {
    $actual = $this->serializer->serialize($this->extension, "json");
    $expected = json_encode([
      "stock_item" => null,
    ]);

    $this->assertJsonStringEqualsJsonString($actual, $expected);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @covers ::setStockItem
   * @covers ::getStockItem
   * @uses Mage\Api\Entities\Catalog\Inventory\StockItemEntity
   */
  public function test_methods_setStockItem_getStockItem ()
  {
    $this->extension->setStockItem(new StockItemEntity);

    $expected = StockItemEntity::class;
    $actual = $this->extension->getStockItem();

    $this->assertInstanceOf($expected, $actual);
  }
}
