<?php

declare(strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\ProductEntityVisibility;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass Mage\Api\Entities\Catalog\Product\ProductEntityVisibility
 * @uses Mage\Api\Entities\EntityStaticValue
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class ProductEntityVisibilitySpec extends TestCase
{
  /**
   * @covers ::getVisibilityLevel
   */
  public function test_visibility_not_visible_alone ()
  {
    $expectedId = 1;
    $actualId = ProductEntityVisibility::NOT_VISIBLE;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Not Visible Individually";
    $actualLabel = ProductEntityVisibility::getVisibilityLevel(1);
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getVisibilityLevel
   */
  public function test_visibility_is_visibile_catalog_only ()
  {
    $expectedId = 2;
    $actualId = ProductEntityVisibility::IN_CATALOG;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Catalog";
    $actualLabel = ProductEntityVisibility::getVisibilityLevel(2);
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getVisibilityLevel
   */
  public function test_visibility_is_visibile_search_only ()
  {
    $expectedId = 3;
    $actualId = ProductEntityVisibility::IN_SEARCH;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Search";
    $actualLabel = ProductEntityVisibility::getVisibilityLevel(3);
    $this->assertEquals($expectedLabel, $actualLabel);
  }

  /**
   * @covers ::getVisibilityLevel
   */
  public function test_visibility_is_visibile_catalog_search ()
  {
    $expectedId = 4;
    $actualId = ProductEntityVisibility::IS_VISIBLE;
    $this->assertEquals($expectedId, $actualId);

    $expectedLabel = "Catalog, Search";
    $actualLabel = ProductEntityVisibility::getVisibilityLevel(4);
    $this->assertEquals($expectedLabel, $actualLabel);
  }
}
