<?php

declare (strict_types = 1);

namespace Mage\Tests\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Inventory\StockItemEntity;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * @coversDefaultClass Mage\Api\Entities\Catalog\Inventory\StockItemEntity
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class StockItemEntitySpec extends TestCase
{
  protected function setUp ()
  {
    $this->stockItem = new StockItemEntity;
    $this->serializer = new Serializer(
      array(
        new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter)
      ),
      array(
        new JsonEncoder
      )
    );
  }

  protected function tearDown ()
  {
    unset($this->stockItem);
    unset($this->serializer);
  }

  /**
   * @coversNothing
   */
  public function test_stock_item_construction_json_serialization ()
  {
    $this->stockItem->setQty(10.67);

    $actual = $this->serializer->serialize($this->stockItem, "json");
    $expected = json_encode([
      "qty" => 10.67,
      "in_stock" => true,
    ]);

    $this->assertJsonStringEqualsJsonString($actual, $expected);
    $this->assertInternalType("string", $actual);
  }

  // /**
  //  * @covers ::setProductId
  //  * @covers ::getProductId
  //  */
  // public function test_methods_setProductId_getProductId ()
  // {
  //   $this->stockItem->setProductId(123);
  //
  //   $expected = 123;
  //   $actual = $this->stockItem->getProductId();
  //
  //   $this->assertEquals($expected, $actual);
  // }

  /**
   * @covers ::setQty
   * @covers ::getQty
   */
  public function test_methods_setQty_getQty ()
  {
    $this->stockItem->setQty(1.24);

    $expected = 1.24;
    $actual = $this->stockItem->getQty();

    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::isInStock
   * @uses Mage\Api\Entities\Catalog\Inventory\StockItemEntity
   */
  public function test_method_isInStock_true ()
  {
    $this->stockItem->setQty(5);
    $expected = true;
    $actual = $this->stockItem->isInStock();

    $this->assertEquals($expected, $actual);
  }

  /**
   * @covers ::isInStock
   * @uses Mage\Api\Entities\Catalog\Inventory\StockItemEntity
   */
  public function test_method_isInStock_false ()
  {
    $this->stockItem->setQty(0);
    $expected = false;
    $actual = $this->stockItem->isInStock();

    $this->assertEquals($expected, $actual);
  }
}
