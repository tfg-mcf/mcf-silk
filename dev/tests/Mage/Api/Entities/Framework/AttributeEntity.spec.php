<?php

declare (strict_types = 1);

namespace Mage\Tests\Api\Entities\Framework;

use Mage\Api\Entities\Framework\AttributeEntity;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * @coversDefaultClass Mage\Api\Entities\Framework\AttributeEntity
 * @author Jordan Brauer <info@jordanbrauer.ca>
 */
class AttributeEntitySpec extends TestCase
{
  protected function setUp ()
  {
    $this->attribute = new AttributeEntity;
    $this->serializer = new Serializer(
      array(
        new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter)
      ),
      array(
        new JsonEncoder
      )
    );
  }

  protected function tearDown ()
  {
    unset($this->attribute);
    unset($this->serializer);
  }

  /**
   * @coversNothing
   */
  public function test_tier_prices_construction_json_serialization ()
  {
    $this->attribute
      ->setAttributeCode("test_attribute")
      ->setValue("1-3/4\"")
      ;

    $actual = $this->serializer->serialize($this->attribute, "json");
    $expected = json_encode([
      "attribute_code" => "test_attribute",
      "value" => "1-3/4\"",
    ]);

    $this->assertJsonStringEqualsJsonString($actual, $expected);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @covers ::setAttributeCode
   * @covers ::getAttributeCode
   */
  public function test_methods_setAttributeCode_getAttributeCode ()
  {
    $this->attribute->setAttributeCode("test_code_attr");

    $expected = "test_code_attr";
    $actual = $this->attribute->getAttributeCode();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }

  /**
   * @covers ::setValue
   * @covers ::getValue
   */
  public function test_methods_setValue_getValue ()
  {
    $this->attribute->setValue("10-7/8\"");

    $expected = "10-7/8\"";
    $actual = $this->attribute->getValue();

    $this->assertEquals($expected, $actual);
    $this->assertInternalType("string", $actual);
  }
}
