<?php

declare(strict_types = 1);

namespace Mage\Tests\Api\Repository\Catalog;

use GuzzleHttp\{ Handler\MockHandler, HandlerStack };
use GuzzleHttp\Psr7\{ Request, Response };
use GuzzleHttp\Exception\RequestException;
use Mage\Api\{ Repository\Catalog\ProductCatalogRepository, Client };
use PHPUnit\Framework\TestCase;

class ProductCatalogRepositorySpec extends TestCase
{
  protected function setUp ()
  {
    $this->mock = new MockHandler(array());
    $this->handler = HandlerStack::create($this->mock);
    $this->mock->append(new Response (200, ["X-Foo" => "Bar"], "1n17_adm1n_70k3n"));
    $this->client = new Client(["handler" => $this->handler]);
    $this->productRepo = new ProductCatalogRepository($this->client);
    $this->productJson = [
      'product' => [
        'status' => 2,
        'type_id' => 'simple',
        'id' => 1,
        'sku' => "SKU123",
        'name' => "Demo Product",
        'price' => 10.25,
        'extension_attributes' => [
          'stock_item' => [
            'qty' => 10,
            'is_in_stock' => true,
          ],
        ],
        'weight' => 3,
        'visibility' => 4,
        'attribute_set_id' => 29,
      ],
      'save_options' => true,
    ];
  }

  protected function tearDown ()
  {
    unset($this->mock);
    unset($this->handler);
    unset($this->client);
    unset($this->productRepo);
  }

  /**
   * @covers Mage\Api\Repository\Catalog\ProductCatalogRepository::addProduct
   * @uses Mage\Api\Repository\Catalog\ProductCatalogRepository
   * @uses Mage\Api\Client
   */
  public function test_method_add_product ()
  {
    $this->mock->append(new Response(200, ["X-Foo" => "Bar"]));
    $request = $this->productRepo->addProduct($this->productJson);
    $this->assertEquals(200, $request->getStatusCode());
    $this->assertEquals("OK", $request->getReasonPhrase());
  }

  /**
   * @covers Mage\Api\Repository\Catalog\ProductCatalogRepository::updateProduct
   * @uses Mage\Api\Repository\Catalog\ProductCatalogRepository
   * @uses Mage\Api\Client
   */
  public function test_method_update_product ()
  {
    $this->mock->append(new Response(200, ["X-Foo" => "Bar"]));
    $request = $this->productRepo->updateProduct("SKU123", $this->productJson);
    $this->assertEquals(200, $request->getStatusCode());
    $this->assertEquals("OK", $request->getReasonPhrase());
  }
}
