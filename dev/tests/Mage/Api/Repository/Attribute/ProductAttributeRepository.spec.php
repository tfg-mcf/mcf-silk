<?php

declare(strict_types = 1);

namespace Mage\Tests\Api\Repository\Attribute;

use GuzzleHttp\{ Handler\MockHandler, HandlerStack };
use GuzzleHttp\Psr7\{ Request, Response };
use GuzzleHttp\Exception\RequestException;
use Mage\Api\{ Repository\Attribute\ProductAttributeRepository, Client };
use PHPUnit\Framework\TestCase;

class ProductAttributeRepositorySpec extends TestCase
{
  public function setUp ()
  {
    $this->mock = new MockHandler(array());
    $this->handler = HandlerStack::create($this->mock);
    $this->mock->append(new Response (200, ["X-Foo" => "Bar"], "1n17_adm1n_70k3n"));
    $this->client = new Client(["handler" => $this->handler]);
    $this->attributeRepo = new ProductAttributeRepository($this->client);
    $this->attributeJson = [
      'option' => [
        'label' => "length",
        'is_default' => false,
      ],
    ];
  }

  public function tearDown ()
  {
    unset($this->mock);
    unset($this->handler);
    unset($this->client);
    unset($this->attributeRepo);
  }

  /**
   * @covers Mage\Api\Repository\Attribute\ProductAttributeRepository::addAttributeOption
   * @uses Mage\Api\Repository\Attribute\ProductAttributeRepository
   * @uses Mage\Api\Client
   */
  public function test_method_add_attribute_option ()
  {
    $this->mock->append(new Response(200, ["X-Foo" => "Bar"]));
    $request = $this->attributeRepo->addAttributeOption("length", $this->attributeJson);
    $this->assertEquals(200, $request->getStatusCode());
    $this->assertEquals("OK", $request->getReasonPhrase());
  }
}
