<?php

declare (strict_types = 1);

namespace Mage\Tests\Api;

use GuzzleHttp\{ Handler\MockHandler, HandlerStack };
use GuzzleHttp\Psr7\{ Request, Response };
use GuzzleHttp\Exception\RequestException;
use Mage\Api\Client;
use PHPUnit\Framework\TestCase;

class ClientSpec extends TestCase
{
  public function setUp ()
  {
    $this->mock = new MockHandler(array());
    $this->handler = HandlerStack::create($this->mock);
    $this->mock->append(new Response (200, ["X-Foo" => "Bar"], "1n17_adm1n_70k3n"));
    $this->client = new Client(["handler" => $this->handler]);
  }

  public function tearDown ()
  {
    unset($this->mock);
    unset($this->handler);
    unset($this->client);
  }

  /**
   * @covers Mage\Api\Client::generateToken
   * @uses Mage\Api\Client
   */
  public function test_client_post_method_generate_token ()
  {
    $this->mock->append(new Response (200, ["X-Foo" => "Bar"], "abc123"));
    $request = $this->client->generateToken("admin", "password");
    $token = $request->getBody()->getContents();

    $this->assertEquals(200, $request->getStatusCode());
    $this->assertEquals("OK", $request->getReasonPhrase());
    $this->assertEquals("abc123", $token);
    $this->assertInternalType("string", $token);
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_get_request_success ()
  {
    $this->mock->append(new Response(200));
    $res200 = $this->client->get("/");
    $this->assertEquals(200, $res200->getStatusCode());
    $this->assertEquals("OK", $res200->getReasonPhrase());

    $this->mock->append(new Response(302));
    $res302 = $this->client->get("/");
    $this->assertEquals(302, $res302->getStatusCode());
    $this->assertEquals("Found", $res302->getReasonPhrase());
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_client_request_exceptions ()
  {
    $this->expectException("GuzzleHttp\Exception\RequestException");
    $this->mock->append(new RequestException("Error communicating with server", new Request("GET", 'test')));
    $this->client->get("/");
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_get_request_client_error_400 ()
  {
    $this->mock->append(new Response(400));
    $this->expectException("GuzzleHttp\Exception\ClientException");
    $this->client->get("/400-bad-request");
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_get_request_client_error_401 ()
  {
    $this->mock->append(new Response(401));
    $this->expectException("GuzzleHttp\Exception\ClientException");
    $this->client->get("/401-unauthorized");
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_get_request_client_error_403 ()
  {
    $this->mock->append(new Response(403));
    $this->expectException("GuzzleHttp\Exception\ClientException");
    $this->client->get("/403-forbidden");
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_get_request_client_error_404 ()
  {
    $this->mock->append(new Response(404));
    $this->expectException("GuzzleHttp\Exception\ClientException");
    $this->client->get("/404-not-found");
  }

  /**
   * @covers Mage\Api\Client::__call
   * @uses Mage\Api\Client
   */
  public function test_get_request_server_error_500 ()
  {
    $this->mock->append(new Response(500));
    $this->expectException("GuzzleHttp\Exception\ServerException");
    $this->client->get("/500-internal-server-error");
  }
}
