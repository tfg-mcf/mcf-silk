<?php

namespace Silk\Commands;

use Silk\{ Console\Command, Utils };
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DevTestsRunCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("dev:tests:run")
      ->setDescription("Run the application test suite from the application itself.")
      ->setHelp("N/A")
      ;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    $io = new SymfonyStyle($input, $output);

    # TODO: Bring in symfony/process
    $io->text(exec("composer test"));
  }
}

