# Silk Road

![mcf-silk](https://img.shields.io/badge/Mid--Canada%20Fasteners-Anzio%20Silk-00bce9.svg)
![version](https://img.shields.io/badge/version-0.0.4-44cc11.svg)
![build status](https://gitlab.com/tfg-mcf/mcf-silk/badges/master/build.svg)
![coverage report](https://gitlab.com/tfg-mcf/mcf-silk/badges/master/coverage.svg)

A command-line application that provides a nice interface between Anzio SILK and Magento 2 to allow for the importing, updating, and normalization of product data.

## Setup

This repository should be installed next to [tfg-mcf/mcf-store](https://gitlab.com/tfg-mcf/mcf-store) on the server.

```shell
$ git clone git@gitlab.com:tfg-mcf/mcf-silk.git silk
$ cd silk && cp ./env.sample.php ./env.php
$ composer install --optimize-autoloader
$ <editor> env.php
```

_Note: replace_ `<editor>` _with your code editor command._

## Usage

Running the following command will list all available Silk Road commands, as well as their explanations, and respective `--help`/`-h` commands.

```shell
$ bin/silk list
```

## Testing

We use PHPUnit for testing Silk Road. If you installed the repository with the `--no-dev` composer flag, you will need to rerun `composer install --optimize-autoloader` (without the `--no-dev` flag) to have access to the test suite.

Simply run the following to test the application,

```shell
$ composer test
```

## Documentation

_Coming soon..._
