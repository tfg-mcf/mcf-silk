<?php

namespace Mage\Product;

/**
 * @deprecated
 */
class Product
{
  public $product;

  public function __construct ()
  {
    $this->product = [
      'status' => 2,
      'attribute_set_id' => 29,
      'weight' => 1,
    ];
  }

  private function addAttribute ($attribute_code, $value)
  {
    $this->product[$attribute_code] = $value;

    return $this;
  }

  private function getAttribute ($attribute_code)
  {
    return $this->product[$attribute_code];
  }

  private function addCustomAttribute ($attribute_code, $value)
  {
    $this->product['custom_attributes'][] = [
      'attribute_code' => $attribute_code,
      'value' => $value
    ];

    return $this;
  }

  private function getCustomAttributes ()
  {
    return $this->getAttribute('custom_attributes');
  }

  public function setMediaGalleryEntries ($media_gallery_entries)
  // (array $images, string $format='jpeg')
  {
    $images = is_array($media_gallery_entries)
      ? $media_gallery_entries
      : array($media_gallery_entries);

    $format = 'jpeg';
    $counter = 0;
    $entries = [];

    foreach ($images as $image):
      $counter++;

      $entry = [
        'position' => $counter,
        'label' => '',
        'media_type' => 'image',
        'disabled' => false,
        'types' => ($counter == 1 ? ['image', 'small_image', 'thumbnail', 'swatch_image'] : []),
        'content' => [
          'type' => "image/{$format}",
          // 'name' => pathinfo($this->getMediaPath().$image, PATHINFO_FILENAME).'.'.pathinfo($this->getMediaPath().$image, PATHINFO_EXTENSION),
          // 'base64_encoded_data' => base64_encode(file_get_contents($this->getMediaPath().$image)),
          'name' => $image,
          'base64_encoded_data' => $image,
        ],
      ];

      array_push($entries, $entry);
    endforeach;

    $this->addAttribute('media_gallery_entries', $entries);

    return $this;
  }

  public function getMediaGalleryEntries ()
  {
    return $this->getAttribute('media_gallery_entries');
  }

  public function setStatus ($status)
  {
    $this->addAttribute('status', $status);

    return $this;
  }

  public function getStatus ()
  {
    return $this->getAttribute('status');
  }

  public function setSku ($sku)
  {
    $this->addAttribute('sku', $sku);

    return $this;
  }

  public function getSku ()
  {
    return $this->getAttribute('sku');
  }

  public function setName ($name)
  {
    $this->addAttribute('name', $name);

    return $this;
  }

  public function getName ()
  {
    return $this->getAttribute('name');
  }

  public function setWeight ($weight)
  {
    $this->addAttribute('weight', $weight);

    return $this;
  }

  public function getWeight ()
  {
    return $this->getAttribute('weight');
  }

  public function setAttributeSetId ($attribute_set_id)
  {
    $this->addAttribute('attribute_set_id', $attribute_set_id);

    return $this;
  }

  public function getAttributeSetId ()
  {
    return $this->getAttribute('attribute_set_id');
  }

  public function setDescription ($description)
  {
    if ($description)
      $this->addCustomAttribute('description', "<h3>OVERVIEW</h3><p>{$description}</p>");

    return $this;
  }

  public function getDescription ()
  {
    return $this->getCustomAttribute('description');
  }

  public function setShortDescription ($short_description)
  {
    if ($short_description)
      $this->addCustomAttribute('short_description', "<h4>WHAT'S INCLUDED</h4><p>{$short_description}</p>");

    return $this;
  }

  public function getShortDescription ()
  {
    return $this->getCustomAttribute('short_description');
  }

  public function setLength ($length)
  {
    if ($length)
      $this->addCustomAttribute('length', $length);

    return $this;
  }

  public function getLength ()
  {
    return $this->getCustomAttribute('length');
  }

  public function setWidth ($width)
  {
    if ($width)
      $this->addCustomAttribute('width', $width);

    return $this;
  }

  public function getWidth ()
  {
    return $this->getCustomAttribute('width');
  }

  public function setHeight ($height)
  {
    if ($height)
      $this->addCustomAttribute('height', $height);

    return $this;
  }

  public function getHeight ()
  {
    return $this->getCustomAttribute('height');
  }

  public function setDiameter ($diameter)
  {
    if ($diameter)
      $this->addCustomAttribute('diameter', $diameter);

    return $this;
  }

  public function getDiameter ()
  {
    return $this->getCustomAttribute('diameter');
  }

  // ============================================
  // PRODUCT DATA ATTRIBUTES
  // ============================================

  public function setPackageQuantity ($package_quantity)
  {
    if ($package_quantity)
      $this->addCustomAttribute('package_quantity', $package_quantity);

    return $this;
  }

  public function getPackageQuantity ()
  {
    return $this->getCustomAttribute('package_quantity');
  }

  public function setFinish ($finish)
  {
    if ($finish)
      $this->addCustomAttribute('finish', $finish);

    return $this;
  }

  public function getFinish ()
  {
    return $this->getCustomAttribute('finish');
  }

  public function setFluteLength ($flute_length)
  {
    if ($flute_length)
      $this->addCustomAttribute('flute_length', $flute_length);

    return $this;
  }

  public function getFluteLength ()
  {
    return $this->getCustomAttribute('flute_length');
  }
}
