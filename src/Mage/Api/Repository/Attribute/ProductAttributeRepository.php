<?php

declare(strict_types = 1);

namespace Mage\Api\Repository\Attribute;

use Mage\Api\Client;

class ProductAttributeRepository
{
  protected $client;

  public function __construct (Client $client)
  {
    $this->client = $client;
  }

  /**
   * Add an option to an attribute set via REST.
   *
   * @param string $attribute A valid attribute code in the Magento database.
   * @param array $json An array of valid Magento API key/values to post.
   * @return Response
   */
  public function addAttributeOption (string $attribute, array $json)
  {
    return $this->client->post("products/attributes/{$attribute}/options", array(
      "headers" => array("Authorization" => "Bearer {$this->client->getToken()}"),
      "json" => $json
    ));
  }
}
