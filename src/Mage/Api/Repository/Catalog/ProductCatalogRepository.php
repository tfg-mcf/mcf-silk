<?php

declare(strict_types = 1);

namespace Mage\Api\Repository\Catalog;

use Mage\Api\Client;

class ProductCatalogRepository
{
  protected $client;

  public function __construct (Client $client)
  {
    $this->client = $client;
  }

  /**
   * Add a new product via REST API
   *
   * @param array $json An arrray of valid Magento API key/values to post
   * @return Response
   */
  public function addProduct (array $json)
  {
    return $this->client->post("products", array(
      "headers" => array("Authorization" => "Bearer {$this->client->getToken()}"),
      "json" => $json
    ));
  }

  /**
   * Update a product via REST.
   *
   * @param string $sku A valid sku in the Magento database.
   * @param array $json An array of valid Magento API key/values to post.
   * @return Response
   */
  public function updateProduct (string $sku, array $json)
  {
    return $this->client->put("products/{$sku}", array(
      "headers" => array("Authorization" => "Bearer {$this->client->getToken()}"),
      "json" => $json,
    ));
  }
}
