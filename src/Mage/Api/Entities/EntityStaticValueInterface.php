<?php

declare(strict_types = 1);

namespace Mage\Api\Entities;

/**
 * Base class for product entity static value classes
 *
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
interface EntityStaticValueInterface
{
  /**
   * Retrieve the human readable version of a static value.
   *
   * @api
   * @param int $key The key/id value to be fetched from the static array.
   * @param array $array A static array to be iterated over for the specified key
   * @return string
   */
  public static function getStaticValue ($key, array $array): string;
}
