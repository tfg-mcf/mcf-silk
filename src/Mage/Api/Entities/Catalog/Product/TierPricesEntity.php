<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Product;

/**
 * Manages a products' tier pricing structure.
 *
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class TierPricesEntity
{
  /**
   * @var int $customer_group_id Customer group id.
   * @var float $qty Tier quantity.
   * @var float $value Tier price value.
   */
  protected $customer_group_id;
  protected $qty;
  protected $value;

  /**
   * Set the tier price customer group ID.
   *
   * @param int $customer_group_id
   * @return $this
   */
  public function setCustomerGroupId (int $customer_group_id): TierPricesEntity
  {
    $this->customer_group_id = $customer_group_id;
    return $this;
  }

  /**
   * Return the tier price customer group ID.
   *
   * @return int
   */
  public function getCustomerGroupId (): int
  {
    return $this->customer_group_id;
  }

  /**
   * Set the tier price qauntity break.
   *
   * @param float $qty
   * @return $this
   */
  public function setQty (float $qty): TierPricesEntity
  {
    $this->qty = $qty;
    return $this;
  }

  /**
   * Return the tier price qauntity break.
   *
   * @return float
   */
  public function getQty (): float
  {
    return $this->qty;
  }

  /**
   * Set the tier price price value.
   *
   * @param float $value
   * @return $this
   */
  public function setValue (float $value): TierPricesEntity
  {
    $this->value = $value;
    return $this;
  }

  /**
   * Return the tier price price value.
   *
   * @return float
   */
  public function getValue (): float
  {
    return $this->value;
  }
}
