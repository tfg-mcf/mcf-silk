<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Product\ExtensionEntity;

/**
 * The base product entity model class for a Magento 2 product.
 * When used in conjunction with the `symfony/serializer` component,
 * we are able to access products as their own objects with this class.
 *
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class ProductEntity
{
  /**
   * @var int $id (Optional) The products' ID.
   * @var string $sku The SKU (Stock Keeping Unit) of the product.
   * @var string $name The name of the product.
   * @var int $attribute_set_id The ID number of the products' attribute set.
   * @var float $price The price of the product.
   * @var int $status The products' status; enabled/disabled.
   * @var int $visibility The visbility level of the product (Not, Catalog, Search, All).
   * @var string $type_id The products' type (simple, configurable, bundle, virtual, etc).
   * @var string $created_at The date/time of the products' creation.
   * @var string $updated_at The date/time of the last update on the product.
   * @var float $weight The weight of the product.
   * @var ProductExtensionEntity $extension_attributes (Optional) An object containing extended product attributes.
   * @var ProductLinkEntity $product_links (Optional) A list of product links (related, up-sells, etc).
   * @var ProductCustomOptionEntity $options An array of product customization options
   * @var ProductAttributeMediaGalleryEntryEntity $media_gallery_entries An array containing the products images/videos.
   * @var ProductTierPriceEntity $tier_prices An array of product tier (level) prices.
   * @var ProductAttributeEntity $custom_attributes An array of custom product attributes.
   */
  protected $id;
  protected $sku;
  protected $name;
  protected $attribute_set_id;
  protected $price;
  protected $status;
  protected $visibility;
  protected $type_id;
  protected $created_at;
  protected $updated_at;
  protected $weight;
  protected $extension_attributes;
  protected $product_links;
  protected $options;
  protected $media_gallery_entries;
  protected $tier_prices;
  protected $custom_attributes;

  /**
   * Set a products' ID number.
   *
   * @param int $id The value to be set as the product ID.
   * @return $this
   */
  public function setId (int $id): ProductEntity
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Return a products' ID number.
   *
   * @return int
   */
  public function getId (): int
  {
    return $this->id;
  }

  /**
   * Set a products' SKU (Stock Keeping Unit) value.
   *
   * @param string $sku The value to be set as the products' SKU.
   * @return $this
   */
  public function setSku (string $sku): ProductEntity
  {
    $this->sku = $sku;
    return $this;
  }

  /**
   * Return a products' SKU value.
   *
   * @return string
   */
  public function getSku (): string
  {
    return $this->sku;
  }

  /**
   * Set the name of a product.
   *
   * @param string $name The value to be set as the products' name.
   * @return $this
   */
  public function setName (string $name): ProductEntity
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Return the name of a product.
   *
   * @return string
   */
  public function getName (): string
  {
    return $this->name;
  }

  /**
   * Set the product attribute set ID.
   *
   * @param int $attribute_set_id The value to be set as the products' attribute set ID.
   * @return $this
   */
  public function setAttributeSetId (int $attribute_set_id): ProductEntity
  {
    $this->attribute_set_id = $attribute_set_id;
    return $this;
  }

  /**
   * Return the product attribute set ID.
   *
   * @return int
   */
  public function getAttributeSetId (): int
  {
    return $this->attribute_set_id;
  }

  /**
   * Set the products price.
   *
   * @param float $price The value to be set as the products' price.
   * @return $this
   */
  public function setPrice (float $price): ProductEntity
  {
    $this->price = $price;
    return $this;
  }

  /**
   * Return the price of a product
   *
   * @return float
   */
  public function getPrice (): float
  {
    return $this->price;
  }

  /**
   * Set the status of the product.
   *
   * @api
   * @param int $status The value to be set as the products' current status.
   * @return $this
   */
  public function setStatus (int $status): ProductEntity
  {
    $this->status = $status;
    return $this;
  }

  /**
   * Return the status of a product
   *
   * @api
   * @return int
   */
  public function getStatus (): int
  {
    return $this->status;
  }

  /**
   * Set the visibility of the product.
   *
   * @api
   * @param int $visibility The value to be set as the products' current visibility.
   * @return $this
   */
  public function setVisibility (int $visibility): ProductEntity
  {
    $this->visibility = $visibility;
    return $this;
  }

  /**
   * Return the visibility of a product.
   *
   * @api
   * @return int
   */
  public function getVisibility (): int
  {
    return $this->visibility;
  }

  /**
   * Set the products' type ID.
   *
   * @api
   * @param string $type_id The value to be set as the products' current type ID.
   * @return $this
   */
  public function setTypeId (string $type_id): ProductEntity
  {
    $this->type_id = $type_id;
    return $this;
  }

  /**
   * Return the products' type ID.
   *
   * @api
   * @return string
   */
  public function getTypeId (): string
  {
    return $this->type_id;
  }

  /**
   * Set the products' "created at" date/time.
   *
   * @api
   * @param string $created_at The products' "created at" date/time.
   * @return $this
   */
  public function setCreatedAt (string $created_at): ProductEntity
  {
    $this->created_at = $created_at;
    return $this;
  }

  /**
   * Return the products' "created at" date/time.
   *
   * @api
   * @return string
   */
  public function getCreatedAt (): string
  {
    return $this->created_at;
  }

  /**
   * Set the products' "updated at" date/time.
   *
   * @api
   * @param string $updated_at The products' "updated at" date/time.
   * @return $this
   */
  public function setUpdatedAt (string $updated_at): ProductEntity
  {
    $this->updated_at = $updated_at;
    return $this;
  }

  /**
   * Return the products' "updated at" date/time.
   *
   * @api
   * @return string
   */
  public function getUpdatedAt (): string
  {
    return $this->updated_at;
  }

  /**
   * Set the products' weight value.
   *
   * @api
   * @param float $weight The products' weight value.
   * @return $this
   */
  public function setWeight (float $weight): ProductEntity
  {
    $this->weight = $weight;
    return $this;
  }

  /**
   * Return the products' weight value.
   *
   * @api
   * @return float
   */
  public function getWeight (): float
  {
    return $this->weight;
  }

  /**
   * Set the products' extension attributes.
   *
   * @api
   * @param ExtensionEntity $extension_attributes The products' extension attributes field.
   * @return $this
   */
  public function setExtensionAttributes (ExtensionEntity $extension_attributes): ProductEntity
  {
    $this->extension_attributes = $extension_attributes;
    return $this;
  }

  /**
   * Return the products' extension attributes field.
   *
   * @api
   * @return ExtensionEntity
   */
  public function getExtensionAttributes (): ExtensionEntity
  {
    return $this->extension_attributes;
  }

  /**
   * Set the products' tier prices.
   *
   * @api
   * @param array $tier_prices The products' tier prices field.
   * @return $this
   */
  public function setTierPrices (array $tier_prices = array()): ProductEntity
  {
    $this->tier_prices = $tier_prices;
    return $this;
  }

  /**
   * Return the products' tier prices field.
   *
   * @api
   * @return array
   */
  public function getTierPrices (): array
  {
    return $this->tier_prices;
  }

  /**
   * Set the products' custom attributes.
   *
   * @api
   * @param array $custom_attributes The products' custom attributes field.
   * @return $this
   */
  public function setCustomAttributes (array $custom_attributes = array()): ProductEntity
  {
    $this->custom_attributes = $custom_attributes;
    return $this;
  }

  /**
   * Return the products' custom attributes field.
   *
   * @api
   * @return array
   */
  public function getCustomAttributes (): array
  {
    return $this->custom_attributes;
  }
}
