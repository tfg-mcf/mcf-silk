<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Product;

use Mage\Api\Entities\EntityStaticValue;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class ProductEntityStatus extends EntityStaticValue
{
   const ENABLED = 1;

   const DISABLED = 2;

   /**
    * @static
    * @var array $status A static array mapping the class constants to their human readable counterparts.
    */
   protected static $statuses = array(
     self::ENABLED => "Enabled",
     self::DISABLED => "Disabled",
   );

   /**
    * Retrieve the human readable version of a status ID.
    *
    * @api
    * @static
    * @param int $statusId A valid product status ID.
    * @return string
    */
   public static function getStatusValue (int $status): string
   {
     return static::getStaticValue($status, static::$statuses);
   }
}
