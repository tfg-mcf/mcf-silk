<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Product;

use Mage\Api\Entities\EntityStaticValue;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class ProductEntityType extends EntityStaticValue
{
   const SIMPLE = "simple";

   const CONFIGURABLE = "configurable";

   const GROUPED = "grouped";

   const VIRTUAL = "virtual";

   const BUNDLE = "bundle";

   const DOWNLOADABLE = "downloadable";

   /**
    * @static
    * @var array $types A static array mapping the class constants to their human readable counterparts.
    */
   protected static $types = array(
     self::SIMPLE => "Simple Product",
     self::CONFIGURABLE => "Configurable Product",
     self::GROUPED => "Grouped Product",
     self::VIRTUAL => "Virtual Product",
     self::BUNDLE => "Bundle Product",
     self::DOWNLOADABLE => "Downloadable Product",
   );

   /**
    * Retrieve the human readable version of a type ID string.
    *
    * @api
    * @static
    * @param int $type A valid product type ID string.
    * @return string
    */
   public static function getTypeValue (string $type): string
   {
     return static::getStaticValue($type, static::$types);
   }
}
