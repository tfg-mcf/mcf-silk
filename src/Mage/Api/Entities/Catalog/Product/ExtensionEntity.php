<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Product;

use Mage\Api\Entities\Catalog\Inventory\StockItemEntity;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class ExtensionEntity
{
  /**
   * @var StockItemEntity $stock_item
   */
  protected $stock_item;

  /**
   * Set the extension entities stock item to an instance of StockItemEntity.
   *
   * @param StockItemEntity $stock_item
   * @return $this
   */
  public function setStockItem (StockItemEntity $stock_item = null): ExtensionEntity
  {
    $this->stock_item = $stock_item;
    return $this;
  }

  /**
   * Set the extension entities stock item to an instance of StockItemEntity.
   *
   * @return StockItemEntity
   */
  public function getStockItem ()//: StockItemEntity
  {
    return $this->stock_item;
  }
}
