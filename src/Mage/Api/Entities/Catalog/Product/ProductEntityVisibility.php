<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Product;

use Mage\Api\Entities\EntityStaticValue;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class ProductEntityVisibility extends EntityStaticValue
{
  const NOT_VISIBLE = 1;

  const IN_CATALOG = 2;

  const IN_SEARCH = 3;

  const IS_VISIBLE = 4;

  /**
   * @static
   * @var array $visibilityLevels A static array mapping the class constants to their human readable counterparts.
   */
  protected static $visibilityLevels = array(
    self::NOT_VISIBLE => "Not Visible Individually",
    self::IN_CATALOG => "Catalog",
    self::IN_SEARCH => "Search",
    self::IS_VISIBLE => "Catalog, Search",
  );

  /**
   * Retrieve the human readable version of a visibility level.
   *
   * @api
   * @static
   * @param int $visibilityLevel A valid visibility level ID.
   * @return string
   */
   public static function getVisibilityLevel (int $visibilityLevel): string
   {
     return static::getStaticValue($visibilityLevel, static::$visibilityLevels);
   }
}
