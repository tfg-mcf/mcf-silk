<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Catalog\Inventory;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class StockItemEntity
{
  /**
   * @var int $item_id
   * @var int $product_id
   * @var int $stock_id Stock identifier.
   * @var float $qty
   * @var bool $is_in_stock Is stock currently available?
   * @var bool $is_qty_decimal
   * @var bool $show_default_notification_message
   * @var bool $use_config_min_qty
   * @var float $min_qty Minimal quantity available for item status in stock.
   * @var bool $use_config_min_sale_qty
   * @var float $min_sale_qty Qty Allowed in Shopping Cart or NULL when there is no limitation.
   * @var bool $use_config_max_sale_qty
   * @var float $max_sale_qty Maximum Qty Allowed in Shopping Cart data wrapper.
   * @var bool $use_config_backorders
   * @var int $backorders Number of current backorders for this product.
   * @var bool $use_config_notify_stock_qty
   * @var float $notify_stock_qty Notify for Quantity Below data wrapper.
   * @var bool $use_config_qty_increments
   * @var float $qty_increments Quantity Increments data wrapper.
   * @var bool $use_config_enable_qty_inc
   * @var bool $enable_qty_increments Whether Quantity Increments is enabled.
   * @var bool $use_config_manage_stock
   * @var bool $manage_stock Can manage stock.
   * @var string $low_stock_date
   * @var bool $is_decimal_divided
   * @var int $stock_status_changed_auto
   * @var ExtensionEntity $extension_attributes
   */
  // protected $item_id;
  // protected $product_id;
  // protected $stock_id;
  protected $qty;
  protected $is_in_stock;
  // protected $is_qty_decimal;
  // protected $show_default_notification_message;
  // protected $use_config_min_qty;
  // protected $min_qty;
  // protected $use_config_min_sale_qty;
  // protected $min_sale_qty;
  // protected $use_config_max_sale_qty;
  // protected $max_sale_qty;
  // protected $use_config_backorders;
  // protected $backorders;
  // protected $use_config_notify_stock_qty;
  // protected $notify_stock_qty;
  // protected $use_config_qty_increments;
  // protected $qty_increments;
  // protected $use_config_enable_qty_inc;
  // protected $enable_qty_increments;
  // protected $use_config_manage_stock;
  // protected $manage_stock;
  // protected $low_stock_date;
  // protected $is_decimal_divided;
  // protected $stock_status_changed_auto;
  // protected $extension_attributes;

  // /**
  //  * Set the current stock item extension product ID.
  //  *
  //  * @param int $product_id An integer matching the current products' ID.
  //  * @return $this
  //  */
  // public function setProductId (int $product_id): StockItemEntity
  // {
  //   $this->product_id = $product_id;
  //   return $this;
  // }

  // /**
  //  * Return the current stock item extension product ID.
  //  *
  //  * @return int
  //  */
  // public function getProductId (): int
  // {
  //   return $this->product_id;
  // }

  /**
   * Set the current stock item extension product ID.
   *
   * @param float $qty A floating point decimal depicting the current stock level.
   * @return $this
   */
  public function setQty (float $qty): StockItemEntity
  {
    $this->qty = $qty;
    return $this;
  }

  /**
   * Return the current stock levels for this product.
   *
   * @return float
   */
  public function getQty (): float
  {
    return $this->qty ?? 0;
  }

  /**
   * Check if the product is in stock or not based on the current stock levels
   *
   * @return bool
   */
  public function isInStock (): bool
  {
    if ($this->getQty() > 0)
      return true;

    return false;
  }
}
