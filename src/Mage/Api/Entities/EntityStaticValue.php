<?php

declare(strict_types = 1);

namespace Mage\Api\Entities;

/**
 * Base class for product entity static value classes
 *
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
abstract class EntityStaticValue implements EntityStaticValueInterface
{
   public static function getStaticValue ($key, array $array): string
   {
     if (!isset($array[$key]))
       throw new \ErrorException("Value '{$key}' is not defined in \$array, use one of: ".implode(', ', array_keys($array)));

     return $array[$key];
   }
}
