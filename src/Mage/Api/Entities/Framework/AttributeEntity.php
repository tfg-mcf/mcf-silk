<?php

declare(strict_types = 1);

namespace Mage\Api\Entities\Framework;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 0.0.1
 */
class AttributeEntity
{
  /**
   * @var string $attribute_code Attribute code.
   * @var string $value Attribute value.
   */
  protected $attribute_code;
  protected $value;

  /**
   * Set the current attribute code.
   *
   * @param string $attribute_code The internal sysytem code for the attribute in snake_case
   * @return $this
   */
  public function setAttributeCode (string $attribute_code): AttributeEntity
  {
    $this->attribute_code = $attribute_code;
    return $this;
  }

  /**
   * Return the current attribute code.
   *
   * @return string
   */
  public function getAttributeCode (): string
  {
    return $this->attribute_code;
  }

  /**
   * Set the current attribute value.
   *
   * @param string $value A value to be set
   * @return $this
   */
  public function setValue (string $value): AttributeEntity
  {
    $this->value = $value;
    return $this;
  }

  /**
   * Return the current attribute value.
   *
   * @return string
   */
  public function getValue (): string
  {
    return $this->value;
  }
}
