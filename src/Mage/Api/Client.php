<?php

declare(strict_types = 1);

namespace Mage\Api;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Client extends GuzzleClient
{
  private $options;

  private $token;

  public function __construct (array $options = array())
  {
    $this->options = $this->configureOptions($options, new OptionsResolver);

    $base_uri = "{$this->options['base_uri']}/{$this->options['endpoint']}/{$this->options['store_id']}/{$this->options['version']}/";

    parent::__construct(array(
      "base_uri" => $base_uri,
      "headers" => $this->options["headers"],
      "handler" => $this->options["handler"],
    ));

    $this->token = $this->setToken();
  }

  /**
   * Configure the client options
   *
   * @param array $options An array containing user defined options
   * @param OptionsResolver $resolver An instance of the Symfony 3 Options Resolver component
   * @return array
   */
  protected function configureOptions (array $options = array(), OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      "base_uri" => "http://127.0.0.1",
      "store_id" => "all",
      "store_ids" => array(),
      "endpoint" => "index.php/rest",
      "version" => "V1",
      "username" => "admin",
      "password" => "password",
      "headers" => array("Content-Type" => "application/json"),
      "handler" => HandlerStack::create(),
    ));
    return $resolver->resolve($options);
  }

  /**
   * Handle any client/server exceptions thrown during API requests
   *
   * @param ClientException $exception The exception object being handled.
   * @return array
   */
  public function handleException (\GuzzleHttp\Exception\ClientException $exception)
  {
    $requestHeaders = array();
    foreach ($exception->getRequest()->getHeaders() as $key => $value):
      $requestHeaders[$key] = $value;
    endforeach;

    $requestBody = json_decode($exception->getRequest()->getBody());
    $responseBody = json_decode($exception->getResponse()->getBody()->getContents());

    return $msg = array(
      $exception->getResponse()->getStatusCode(),
      json_encode($responseBody, JSON_PRETTY_PRINT),
      "[REQUEST HEADERS]",
      json_encode($requestHeaders, JSON_PRETTY_PRINT),
      "[REQUEST BODY]",
      json_encode($requestBody, JSON_PRETTY_PRINT),
    );
  }

  /**
   * Set the client admin token
   */
  public function setToken ()
  {
    $response = $this->generateToken($this->options["username"], $this->options["password"]);
    return $this->token = json_decode($response->getBody()->getContents());
  }

  public function getToken ()
  {
    return $this->token;
  }

  /**
   * Generate a Magento 2 admin authorization token
   *
   * @param string $username A valid user from the Magento database.
   * @param string $password The correct password that identifies the defined user.
   * @return string
   */
  public function generateToken (string $username, string $password)
  {
    return $this->post("integration/admin/token", array(
      "headers" => array("Authorization" => "Bearer {$this->token}"),
      "json" => array(
        "username" => $username,
        "password" => $password,
      )
    ));
  }

  /**
   * Add a new product via REST API
   *
   * @deprecated
   *
   * @param array $json An arrray of valid Magento API key/values to post
   * @return Response
   */
  public function addProduct (array $json)
  {
    \Silk\Utils::deprecate("Please use the new Mage\Catalog\ProductCatalogRepository class");
    return $this->post("products", array(
      "headers" => array("Authorization" => "Bearer {$this->token}"),
      "json" => $json
    ));
  }

  /**
   * Update a product via REST.
   *
   * @deprecated
   *
   * @param string $sku A valid sku in the Magento database.
   * @param array $json An array of valid Magento API key/values to post.
   * @return Response
   */
  public function updateProduct (string $sku, array $json)
  {
    \Silk\Utils::deprecate("Please use the new Mage\Catalog\ProductCatalogRepository class");
    return $this->put("products/{$sku}", array(
      "headers" => array("Authorization" => "Bearer {$this->token}"),
      "json" => $json,
    ));
  }

  /**
   * Add an option to an attribute set via REST.
   *
   * @deprecated
   *
   * @param string $attribute A valid attribute code in the Magento database.
   * @param array $json An array of valid Magento API key/values to post.
   * @return Response
   */
  public function addAttributeOption (string $attribute, array $json)
  {
    \Silk\Utils::deprecate("Please use the new Mage\Attribute\ProductAttributeRepository class");
    return $this->post("products/attributes/{$attribute}/options", array(
      "headers" => array("Authorization" => "Bearer {$this->token}"),
      "json" => $json
    ));
  }

  /**
   * Kill the API client.
   *
   * @deprecated
   */
  public function kill ()
  {
    \Silk\Utils::deprecate("The Mage\Api\Client::kill() method no longer does anything. Please remove all calls to it.");
  }
}
