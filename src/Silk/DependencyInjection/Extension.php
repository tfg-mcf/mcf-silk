<?php

namespace Silk\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension as SymfonyExtension;

class Extension extends SymfonyExtension
{
  public function load (array $configs, ContainerBuilder $container)
  {
    $serviceLocator = new FileLocator(ROOT_DIR);

    $serviceLoader = new DirectoryLoader($container, $serviceLocator);

    $serviceLoader->setResolver(new LoaderResolver(array(
      new PhpFileLoader($container, $serviceLocator),
      $serviceLoader,
    )));

    # FIXME: hard coded services directory
    $serviceLoader->load("app/services/");
  }
}
