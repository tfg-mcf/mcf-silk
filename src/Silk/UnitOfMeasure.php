<?php

namespace Silk;

class UnitOfMeasure
{
  const BOX = 1;

  const EACH = 1;

  const PAIR = 1;

  const CASE = 1;

  const REEL = 1;

  const FEET = 1;

  const PER_C = 100;

  const PER_M = 1000;

  protected static $unitValues = array(
    "BX" => self::BOX,
    "EA" => self::EACH,
    "PR" => self::PAIR,
    "CS" => self::CASE,
    "RL" => self::REEL,
    "FT" => self::FEET,
    "/C" => self::PER_C,
    "/M" => self::PER_M,
  );

  protected static $unitNames = array(
    "BX" => "BOX",
    "EA" => "EACH",
    "PR" => "PAIR",
    "CS" => "CASE",
    "RL" => "REEL",
    "FT" => "FEET",
    "/C" => "/C",
    "/M" => "/M",
  );

  # NOTE: Monolog method - problem is that we have duplicated keys due to aliased units
  // protected static $units = array(
  //   self::BOX => "BX",
  //   self::EACH => "EA",
  //   self::PAIR => "PR",
  //   self::CASE => "CS",
  //   self::REEL => "RL",
  //   self::FEET => "FT",
  //   self::PER_C => "/C",
  //   self::PER_M => "/M",
  // );

  public static function getName (string $unit): string
  {
    if (!isset(static::$unitNames[$unit]))
      throw new \ErrorException("Unit '{$unit}' is not defined, use one of: ".implode(', ', array_keys(static::$unitNames)));

    return static::$unitNames[$unit];
  }

  public static function getValue (string $unit): int
  {
    if (!isset(static::$unitValues[$unit]))
      throw new \ErrorException("Unit '{$unit}' is not defined, use one of: ".implode(', ', array_keys(static::$unitValues)));

    return static::$unitValues[$unit];
  }
}
