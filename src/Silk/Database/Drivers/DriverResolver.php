<?php

declare(strict_types = 1);

namespace Silk\Database\Drivers;

use PDO;
use Silk\Database\ClientInterface;

class DriverResolver implements DriverResolverInterface
{
  /**
   * @var Client $client The database client to connect to
   */
  protected $client;

  /**
   * The public constructor for this resolver
   *
   * @param ClientInterface $client A client object that implements ClientInterface
   */
  public function __construct (ClientInterface $client)
  {
    $this->client = $client;
  }

  /**
   * Resolve the PDO instantiation arguments depeneing on the current driver
   *
   * @return PDO
   */
  public function resolve (): PDO
  {
    switch ($this->client->getDriver()):
      case "mysql":
        return new PDO(
          $this->client->getDSN(),
          $this->client->getUsername(),
          $this->client->getPassword()
        );
        break;
      default:
        return new PDO($this->client->getDSN());
    endswitch;
  }
}
