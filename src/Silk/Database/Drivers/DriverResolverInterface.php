<?php

namespace Silk\Database\Drivers;

use PDO;
use Silk\Database\ClientInterface;

interface DriverResolverInterface
{
  /**
   * The public constructor for this resolver
   *
   * @param ClientInterface $client A client object that implements ClientInterface
   */
  public function __construct (ClientInterface $client);

  /**
   * Resolve the PDO instantiation arguments depeneing on the current driver
   *
   * @return PDO
   */
  public function resolve (): PDO;
}
