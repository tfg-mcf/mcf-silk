<?php

namespace Silk\Database;

interface ClientInterface
{
  public function connect ();

  public function disconnect ();

  public function getConnectionStatus();

  public function getDSN(): string;

  public function getDriver(): string;

  public function getHost(): string;

  public function getPort(): int;

  public function getSchema(): string;

  public function getUsername(): string;

  public function getPassword(): string;
}
