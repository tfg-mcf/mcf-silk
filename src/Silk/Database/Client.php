<?php

declare(strict_types = 1);

namespace Silk\Database;

use Silk\Database\Drivers\DriverResolver;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Client implements ClientInterface
{
  /**
   * @var string $driver The database driver being used
   * @var string $host The database host being connected to
   * @var int $port The port number that the database is accessible through
   * @var string $schema The database schema to use upon connection
   * @var string $username The user that is executing CRUD operations on the database
   * @var string $password The password that identifies the database user
   * @var string $dsn The fully constructed Data Source Name for the database connection
   * @var PDO $connection The constructed connection object
   */
  protected $driver;
  protected $host;
  protected $port;
  protected $schema;
  protected $username;
  protected $password;
  protected $dsn;
  protected $connection;

  /**
   * Public constructor for the client object
   *
   * @param array $options An array containing valid DSN key/value option pairs
   */
  public function __construct (array $options = array())
  {
    $options = $this->configureOptions($options, new OptionsResolver);

    $this
      ->setDriver($options["driver"])
      ->setHost($options["host"])
      ->setPort($options["port"])
      ->setSchema($options["schema"])
      ->setUsername($options["username"])
      ->setPassword($options["password"])
      ->setDSN()
      ;

    $this->connect();
  }

  /**
   * Configures the clients options and settings for a connection
   *
   * @param array $options An array of options containing only valid key/value pairs
   * @param OptionsResolver $resolver An instance of the Symfony 3 component Options Resolver
   * @return array
   */
  protected function configureOptions (array $options = array(), OptionsResolver $resolver): array
  {
    $resolver->setDefaults(array(
      "driver" => "mysql",
      "host" => "localhost",
      "port" => 3306,
      "schema" => "demo_schema",
      "username" => "root",
      "password" => "root",
    ));

    return $resolver->resolve($options);
  }

  /**
   * Construct the full database DSN (Data Source Name) string
   * This method should be called __after__ the rest of the setters.
   *
   * @return null
   */
  protected function setDSN ()
  {
    switch ($this->getDriver()):
      case "mysql":
        $this->dsn = "{$this->getDriver()}:host={$this->getHost()};port={$this->getPort()};dbname={$this->getSchema()}";
        break;
      default:
        $this->dsn = "{$this->getDriver()}:host={$this->getHost()};port={$this->getPort()};dbname={$this->getSchema()};user={$this->getUsername()};password={$this->getPassword()}";
    endswitch;
  }

  /**
   * Set the current client database driver
   *
   * @param string $driver A qualified database driver type
   * @return $this
   */
  protected function setDriver (string $driver): Client
  {
    $this->driver = $driver;
    return $this;
  }

  /**
   * Set the current client database host
   *
   * @param string $host An IP address or FQDN (Fully Qualified Domain Name) where the database lives
   * @return $this
   */
  protected function setHost (string $host): Client
  {
    $this->host = $host;
    return $this;
  }

  /**
   * Set the current client database port
   *
   * @param int $port The port number that your connection is accessable through
   * @return $this
   */
  protected function setPort (int $port): Client
  {
    $this->port = $port;
    return $this;
  }

  /**
   * Set the current client database schema
   *
   * @param string $schema The schema (database name) to be connected to
   * @return $this
   */
  protected function setSchema (string $schema): Client
  {
    $this->schema = $schema;
    return $this;
  }

  /**
   * Set the client connection username
   *
   * @param string $username The username to use while connecting to the database
   * @return $this
   */
  protected function setUsername (string $username): Client
  {
    $this->username = $username;
    return $this;
  }

  /**
   * Set the client connection password
   *
   * @param string $password The password used to identify the username while connecting to the database
   * @return $this
   */
  protected function setPassword (string $password): Client
  {
    $this->password = $password;
    return $this;
  }

  /**
   * Make a connection to the currently configured database
   *
   * @return null|$this
   */
  public function connect ()
  {
    if ($this->connection)
      return $this;

    $driverResolver = new DriverResolver($this);
    $this->connection = $driverResolver->resolve();
  }

  /**
   * Disconnect from the currently connected database
   *
   * @return null
   */
  public function disconnect ()
  {
    $this->connection = null;
  }

  /**
   * Returns the current client connection status
   *
   * @api
   * @return string
   */
  public function getConnectionStatus()
  {
    if ($this->connection)
      return $this->connection->getAttribute(\PDO::ATTR_CONNECTION_STATUS);

    return null;
  }

  /**
   * Return the fully constructed client DSN (Data Source Name)
   *
   * @api
   * @return string
   */
  public function getDSN (): string
  {
    return $this->dsn;
  }

  /**
   * Returns the current client database driver
   *
   * @api
   * @return string
   */
  public function getDriver (): string
  {
    return $this->driver;
  }

  /**
   * Returns the current client database hostname
   *
   * @api
   * @return string
   */
  public function getHost (): string
  {
    return $this->host;
  }

  /**
   * Returns the current client database port number
   *
   * @api
   * @return int
   */
  public function getPort (): int
  {
    return $this->port;
  }

  /**
   * Returns the current schema
   *
   * @api
   * @return int
   */
  public function getSchema (): string
  {
    return $this->schema;
  }

  /**
   * Get the current client connection username
   *
   * @api
   * @return string
   */
  public function getUsername (): string
  {
    return $this->username;
  }

  /**
   * Get the current client connection password
   *
   * @api
   * @return string
   */
  public function getPassword (): string
  {
    return $this->password;
  }
}
