<?php

namespace Silk\Database\Entities;

/**
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 8.3b
 */
abstract class AbstractEntity
{
  const VERSION = "8.3b";
}
