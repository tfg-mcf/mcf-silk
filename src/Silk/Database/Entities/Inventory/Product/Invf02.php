<?php

namespace Silk\Database\Entities\Inventory\Product;

use Silk\Database\Entities\AbstractEntity;

/**
 * File contains descriptions, pricing, flags for all inventory items.
 * Location-specific information is located in `Invf12`.
 *
 * @link https://support.silksystems.com/data_dictionary.php?mode=filedetail&file=invf02
 *
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 8.3b
 */
class Invf02 extends AbstractEntity
{
  /**
   * @var string $itemcode The product SKU. Max 15 characters.
   */
  protected $itemcode;

  /**
   * @var string $description The first line of the products' description. Max length 25.
   * @var string $descriptionline2 The second line of the products' description. Max length 25.
   * @var string $descriptionline3 The third line of the products' description. Max length 25.
   * @var string $descriptionline4 The fourth line of the products' description. Max length 24.
   */
  protected $description;
  protected $descriptionline2;
  protected $descriptionline3;
  protected $descriptionline4;

  /**
   * @var string $itemsize The size of the item. Max length 6.
   */
  protected $itemsize;

  /**
   * @var string $itemtype This items' type. Max length 6. (I=instock, N=non-stock, G=*GST/TAX2 adjustment item [charge], P=*PST/TAX1 adjustment item [charge], M=*misc item, F=*freight, D=discontinued). * Do not track or record qty on hand, rsrvd, backorder, avg cost, and min/max amounts.
   */
  protected $itemtype;

  /**
   * @var string $itemcategory The cateogry of the product. Max length 2.
   * @var string $itemdivision The division of the product. Max length 3.
   */
  protected $itemcategory;
  protected $itemdivision;

  /**
   * @var string $i_filler __Unknown use__. Max length 8.
   * @var string $i_filler_2 __Unknown use__. Max length 18.
   * @var string|Date $i_filler_3 __Unknown use__. Max length 11.
   * @var string $i_bo_flg_bck __Unknown use__. Max length 1.
   */
  protected $i_filler;
  protected $i_filler_2;
  protected $i_filler_3;
  protected $i_bo_flg_bck;

  /**
   * @var string $datecreated The date this item was created. Max 6 characters.
   * @var string $timecreated The timestamp of this item when it was created. Max 4 characters.
   * @var string $createdby The user that this item was created by. Max 4 characters.
   */
  protected $datecreated;
  protected $timecreated;
  protected $createdby;

  /**
   * @var string $dateupdated The date this item was updated. Max 6 characters.
   * @var string $timeupdated The timestamp of this item when it was updated. Max 4 characters.
   * @var string $updatedby The user that this item was updated by. Max 4 characters.
   */
  protected $dateupdated;
  protected $timeupdated;
  protected $updatedby;

  /**
   * @var string $firstvendorcode The primary vendor of this product. Max length 5. Links to `acpf01`.
   * @var string $secondvendorcode The secondary vendor of this product Max length 5. Links to `acpf01`.
   */
  protected $firstvendorcode;
  protected $secondvendorcode;

  /**
   * @var string $transferflag __Unknown use__. Max length 1.
   * @var string $priceoverrideflag __Unknown use__. Max length 1.
   * @var string $kitflag __Unknown use__. Max length 1.
   * @var string $serialized __Unknown use__. Max length 1.
   */
  protected $transferflag;
  protected $priceoverrideflag;
  protected $kitflag;
  protected $serialized;

  /**
   * @var string $alternateu_m The products alternative unit of measure. Max length 2.
   * @var string $fisrtu_m The products primary unit of measure. Max length 2.
   * @var string $secondu_m The products secondary unit of measure. Max length 2.
   * @var int $preferu_m The default prefered unit of measure for this product. Max length 1.
   * @var int $u_mconversionfactor __Unknown use__. Max length 6.
   * @var int $conv_factorforalt_u_m __Unknown use__. Max length 6.
   */
  protected $alternateu_m;
  protected $fisrtu_m;
  protected $secondu_m;
  protected $preferu_m;
  protected $u_mconversionfactor;
  protected $conv_factorforalt_u_m;

  /**
   * @var int $unitweight The weight of the products unit. Max length 6.
   */
  protected $unitweight;

  /**
   * @var int $averagecost The average internal cost paid for this product. Max 6 characters.
   * @var int $commissioncost __Unknown use__. Max 6 characters.
   * @var string $zerocost __Unknown use__. Max characters 1.
   * @var int $lastcost1stu_m The cost of this item when it was last purchased based on 1st UoM. Max 6 characters.
   * @var int $lastcostpct1stu_m The percentage cost of this item when it was last purchased based on 1st UoM. Max 2 characters.
   * @var int $lastcost2ndu_m The cost of this item when it was last purchased based on 2nd UoM. Max 6 characters.
   * @var int $lastcostpct2ndu_m The percentage cost of this item when it was last purchased based on 2nd UoM. Max 2 characters.
   */
  protected $averagecost;
  protected $commissioncost;
  protected $zerocost;
  protected $lastcost1stu_m;
  protected $lastcostpct1stu_m;
  protected $lastcost2ndu_m;
  protected $lastcostpct2ndu_m;

  /**
   * @var string $pricecode The products' price code. Max length 1.
   */
  protected $pricecode;

  /**
   * @var int $regularpricepct1stu_m The products' primary regular percentage price. Max 2 characters.
   * @var int $regularprice1stu_m  The products' primary regular price amount. Max 6 characters.
   * @var int $regularpricepct2ndu_m The products' secondary regular percentage price Max 2 characters.
   * @var int $regularprice2ndu_m  The products' secondary regular price amount Max 6 characters.
   */
  protected $regularpricepct1stu_m;
  protected $regularprice1stu_m;
  protected $regularpricepct2ndu_m;
  protected $regularprice2ndu_m;

  /**
   * @var int $retailpct1stu_m  The products' primary retail percentage price. Max 2 characters.
   * @var int $retailprice1stu_m  The products' primary retail price amount. Max 6 characters.
   * @var int $retailpct2ndu_m  The products' secondary retail percentage price Max 2 characters.
   * @var int $retailprice2ndu_m  The products' secondary retail price amount Max 6 characters.
   */
  protected $retailpct1stu_m;
  protected $retailprice1stu_m;
  protected $retailpct2ndu_m;
  protected $retailprice2ndu_m;

  /**
   * @var int $pricelvlapct1stu_m  The products' primary level A price percentage. Max 2 characters.
   * @var int $pricea1stu_m  The products' primary level A price amount. Max 6 characters.
   * @var int $qtybreaklvla1stu_m  The products' primary level A quantity break amount. Max 3 characters.
   * @var int $pricelvlapct2ndu_m  The products' secondary level A price percentage Max 2 characters.
   * @var int $pricea2ndu_m  The products' secondary level A price amount Max 6 characters.
   * @var int $qtybreaklvla2ndu_m  The products' secondary level A quantity break amount Max 3 characters.
   */
  protected $pricelvlapct1stu_m;
  protected $pricea1stu_m;
  protected $qtybreaklvla1stu_m;
  protected $pricelvlapct2ndu_m;
  protected $pricea2ndu_m;
  protected $qtybreaklvla2ndu_m;

  /**
   * @var int $pricelvlbpct1stu_m  The products' primary level A price percentage. Max 2 characters.
   * @var int $priceb1stu_m  The products' primary level A price amount. Max 6 characters.
   * @var int $qtybreaklvlb1stu_m  The products' primary level A quantity break amount. Max 3 characters.
   * @var int $pricelvlbpct2ndu_m  The products' secondary level A price percentage Max 2 characters.
   * @var int $priceb2ndu_m  The products' secondary level A price amount Max 6 characters.
   * @var int $qtybreaklvlb2ndu_m  The products' secondary level A quantity break amount Max 3 characters.
   */
  protected $pricelvlbpct1stu_m;
  protected $priceb1stu_m;
  protected $qtybreaklvlb1stu_m;
  protected $pricelvlbpct2ndu_m;
  protected $priceb2ndu_m;
  protected $qtybreaklvlb2ndu_m;

  /**
   * @var int $pricelvlcpct1stu_m  The products' primary level A price percentage. Max 2 characters.
   * @var int $pricec1stu_m  The products' primary level A price amount. Max 6 characters.
   * @var int $qtybreaklvlc1stu_m  The products' primary level A quantity break amount. Max 3 characters.
   * @var int $pricelvlcpct2ndu_m  The products' secondary level A price percentage Max 2 characters.
   * @var int $pricec2ndu_m  The products' secondary level A price amount Max 6 characters.
   * @var int $qtybreaklvlc2ndu_m  The products' secondary level A quantity break amount Max 3 characters.
   */
  protected $pricelvlcpct1stu_m;
  protected $pricec1stu_m;
  protected $qtybreaklvlc1stu_m;
  protected $pricelvlcpct2ndu_m;
  protected $pricec2ndu_m;
  protected $qtybreaklvlc2ndu_m;

  /**
   * @var int $pricelvldpct1stu_m  The products' primary level A price percentage. Max 2 characters.
   * @var int $priced1stu_m  The products' primary level A price amount. Max 6 characters.
   * @var int $qtybreaklvld1stu_m  The products' primary level A quantity break amount. Max 3 characters.
   * @var int $pricelvldpct2ndu_m  The products' secondary level A price percentage Max 2 characters.
   * @var int $priced2ndu_m  The products' secondary level A price amount Max 6 characters.
   * @var int $qtybreaklvld2ndu_m  The products' secondary level A quantity break amount Max 3 characters.
   */
  protected $pricelvldpct1stu_m;
  protected $priced1stu_m;
  protected $qtybreaklvld1stu_m;
  protected $pricelvldpct2ndu_m;
  protected $priced2ndu_m;
  protected $qtybreaklvld2ndu_m;

  /**
   * @var int $pricelvlepct1stu_m  The products' primary level A price percentage. Max 2 characters.
   * @var int $pricee1stu_m  The products' primary level A price amount. Max 6 characters.
   * @var int $qtybreaklvle1stu_m  The products' primary level A quantity break amount. Max 3 characters.
   * @var int $pricelvlepct2ndu_m  The products' secondary level A price percentage Max 2 characters.
   * @var int $pricee2ndu_m  The products' secondary level A price amount Max 6 characters.
   * @var int $qtybreaklvle2ndu_m  The products' secondary level A quantity break amount Max 3 characters.
   */
  protected $pricelvlepct1stu_m;
  protected $pricee1stu_m;
  protected $qtybreaklvle1stu_m;
  protected $pricelvlepct2ndu_m;
  protected $pricee2ndu_m;
  protected $qtybreaklvle2ndu_m;

  /**
   * @var string $specialfeature __Unknown use__. Max length 1.
   */
  protected $specialfeature;

  /**
   * @var string $binlocation The products' primary bin location. Max 5 characters.
   * @var string $secondbinlocation The products' secondary bin location. Max 5 characters.
   * @var string $thirdbinlocation The products' tertiary bin location. Max 5 characters.
   * @var string $fourthbinlocation The products' quaternary bin location. Max 5 characters.
   */
  protected $binlocation;
  protected $secondbinlocation;
  protected $thirdbinlocation;
  protected $fourthbinlocation;

  /**
   * @var string $gsttaxable Is this item GST taxable? Max 1 characters.
   * @var string $psttaxable Is this item PST taxable? Max 1 character
   */
  protected $gsttaxable;
  protected $psttaxable;

  /**
   * @var string $reservedflag Is this item currently in reserve? Max 1 character.
   * @var string $awardflag __Unknown use__. Max 1 character.
   * @var string $changedescriptionflag __Unknown use__. Max 1 character.
   */
  protected $reservedflag;
  protected $awardflag;
  protected $changedescriptionflag;

  /**
   * @var string $coretracking __Unknown use__. Max 1 character.
   */
  protected $coretracking;

  /**
   * @var int $thickness __Unknown use__. Max 6 characters.
   * @var int $width __Unknown use__. Max 6 characters.
   */
  protected $thickness;
  protected $width;

  /**
   * @var int $refundamount __Unknown use__. Max 6 characters.
   */
  protected $refundamount;

  /**
   * @var string $return_toitemcode __Unknown use__. Max 15 characters.
   */
  protected $return_toitemcode;

  /**
   * @var string $suggestsubstitutes __Unknown use__. Max 1 character.
   */
  protected $suggestsubstitutes;

  /**
   * @var string $marketingclass __Unknown use__. Max 2 characters.
   */
  protected $marketingclass;

  /**
   * @var string $lastvendorpurchasedfrm The vendor code of the vendor that was last purchased from. Max 5 characters. Links to `acpf01`.
   */
  protected $lastvendorpurchasedfrm;

  /**
   * @var string $brewschedule __Unknown use__. Max characters 3. Links to `schf01`.
   */
  protected $brewschedule;

  /**
   * @var string $volumepromptpaydscnt __Unknown use__. Max characters 1.
   */
  protected $volumepromptpaydscnt;

  /**
   * @var string $printmemooninvoice Print a memo on invoicing of this item. Max characters 1.
   */
  protected $printmemooninvoice;

  /**
   * @var string $inventoryratecode __Unknown use__. Max characters 1.
   */
  protected $inventoryratecode;

  /**
   * @var string $backorderallowed Are back orders allowed on this item? Max characters 1.
   */
  protected $backorderallowed;

  /**
   * @var string $inventoryclass1 __Unknown use__. Max characters 6.
   * @var string $inventoryclass2 __Unknown use__. Max characters 6.
   * @var string $inventoryclass3 __Unknown use__. Max characters 6.
   * @var string $inventoryclass4 __Unknown use__. Max characters 6.
   */
  protected $inventoryclass1;
  protected $inventoryclass2;
  protected $inventoryclass3;
  protected $inventoryclass4;

  /**
   * @var int $inventorycomment Link to inventory comment, comment itself not available in SQL at this time. Max length 4.
   */
  protected $inventorycomment;

  /**
   * @var int $bonusepoint __Unknown use__. Max characters 5.
   */
  protected $bonusepoint;

  /**
   * @var int $notused __Unknown use__. Max characters 6.
   */
  protected $notused;

  /**
   * @var int $grossmarginpct __Unknown use__. Max characters 6.
   */
  protected $grossmarginpct;

  /**
   * @var string $printlabel __Unknown use__. Max characters 1.
   */
  protected $printlabel;

  /**
   * @var int $lifescan __Unknown use__. Max length 6.
   */
  protected $lifescan;
}
