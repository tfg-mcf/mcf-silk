<?php

namespace Silk\Database\Entities\Inventory\Product;

use Silk\Database\Entities\AbstractEntity;

/**
 * File contains Inventory Activities that includes On Hand Quantity/Back-ordered,
 * Reserved/Minimum/Maximum and Location cost and Price Option Flag. If Price Option
 * is Set to "Y"(Selling Price by Location) then it will also have the different
 * Levels of Selling Price.
 *
 * @link https://support.silksystems.com/data_dictionary.php?mode=filedetail&file=invf12
 *
 * @author Jordan Brauer <info@jordanbrauer.ca>
 * @version 8.3b
 */
class Invf12 extends AbstractEntity
{
  /**
   * @var string $itemcode The product SKU. Max 15 characters. Links to `invf02`.
   */
  protected $itemcode;

  /**
   * @var string $locationkey The store that the item belongs to. Max 2 characters. Links to `invf11`.
   * @var string $locationcode The location that this item belongs to. Max 2 characters.
   * @var string $warehousecode The warehouse that this item belongs to. Max 6 characters.
   */
 protected $locationkey;
 protected $locationcode;
 protected $warehousecode;

  /**
   * @var int $quantityonhand The amount of stock currently on hand. Max 6 characters.
   * @var int $minimumqtylevel The minimum amount of stock allowed on hand. Max 6 characters.
   * @var int $maximumqtylevel The maximum amount of stock allowed on hand. Max 6 characters.
   * @var int $quantitybackordered The amount of stock currently backordered. Max 6 characters.
   * @var int $quantityreserved The amount of stock currently within orders. Max 6 characters.
   * @var int $quantityintransit __Unknown use__. Max 6 characters.
   * @var int $qtyonorder The amount of stock that is currently on order. Max 6 characters.
   */
  protected $quantityonhand;
  protected $minimumqtylevel;
  protected $maximumqtylevel;
  protected $quantitybackordered;
  protected $quantityreserved;
  protected $quantityintransit;
  protected $qtyonorder;

  /**
   * @var string $gsttaxableflag Is this item GST taxable? Max 1 characters.
   * @var string $gsttaxcode This items GST tax code. Max 3 characters. Links to `sysf11`.
   * @var string $psttaxable Is this item PST taxable? Max 1 character
   * @var string $psttaxcode1 This items 1st PST tax code. Max 3 characters. Links to `sysf11`.
   * @var string $psttaxcode2 This items 2nd PST tax code. Max 3 characters. Links to `sysf11`.
   * @var string $psttaxcode3 This items 3rd PST tax code. Max 3 characters. Links to `sysf11`.
   * @var string $psttaxcode4 This items 4th PST tax code. Max 3 characters. Links to `sysf11`.
   */
  protected $gsttaxableflag;
  protected $gsttaxcode;
  protected $psttaxable;
  protected $psttaxcode1;
  protected $psttaxcode2;
  protected $psttaxcode3;
  protected $psttaxcode4;

  /**
   * @var string $unused01 __Unused__. Max 17 characters.
   */
  protected $unused01;

  /**
   * @var string $binlocation The products' primary bin location. Max 6 characters.
   * @var string $secondbinlocation The products' secondary bin location. Max 6 characters.
   * @var string $thirdbinlocation The products' tertiary bin location. Max 6 characters.
   * @var string $fourthbinlocation The products' quaternary bin location. Max 6 characters.
   */
  protected $binlocation;
  protected $secondbinlocation;
  protected $thirdbinlocation;
  protected $fourthbinlocation;

  /**
   * @var int $commissioncost __Unknown use__. Max 6 characters.
   * @var int $averagecost The average internal cost paid for this product. Max 6 characters.
   * @var string $lastsalesdate The last date this item was sold. Max 6 characters.
   * @var string $lastpurchasedate The last date that this item was purchased. Max 6 characters.
   * @var int $lastcost The cost of this item when it was last purchased. Max 6 characters.
   * @var int $lastp_oexchange __Unknown use__. Max 6 characters.
   * @var int $lastp_oduty __Unknown use__. Max 6 characters.
   * @var int $lastp_ofreight __Unknown use__. Max 6 characters.
   * @var int $lastp_opurchase __Unknown use__. Max 6 characters.
   * @var string $lastvendorpurchasedfrm The vendor code of the vendor that was last purchased from. Max 5 characters. Links to `acpf01`.
   * @var string $priceoption __Unknown use__. Max 1 characters.
   */
  protected $commissioncost;
  protected $averagecost;
  protected $lastsalesdate;
  protected $lastpurchasedate;
  protected $lastcost;
  protected $lastp_oexchange;
  protected $lastp_oduty;
  protected $lastp_ofreight;
  protected $lastp_opurchase;
  protected $lastvendorpurchasedfrm;
  protected $priceoption;

  /**
   * @var int $costpct1 The products' primary percentage cost. Max 2 characters.
   * @var int $costamount1 The products' primary cost amount. Max 6 characters.
   * @var int $costpct2 The products' secondary percentage cost Max 2 characters.
   * @var int $costamount2 The products' secondary cost amount Max 6 characters.
   */
  protected $costpct1;
  protected $costamount1;
  protected $costpct2;
  protected $costamount2;

  /**
   * @var int $regpricepct1 The products' primary regular percentage price. Max 2 characters.
   * @var int $regularprice1  The products' primary regular price amount. Max 6 characters.
   * @var int $regpricepct2 The products' secondary regular percentage price Max 2 characters.
   * @var int $regularprice2  The products' secondary regular price amount Max 6 characters.
   */
  protected $regpricepct1;
  protected $regularprice1;
  protected $regpricepct2;
  protected $regularprice2;

  /**
   * @var int $retailpricepct1  The products' primary retail percentage price. Max 2 characters.
   * @var int $retailprice1  The products' primary retail price amount. Max 6 characters.
   * @var int $retailpricepct2  The products' secondary retail percentage price Max 2 characters.
   * @var int $retailprice2  The products' secondary retail price amount Max 6 characters.
   */
  protected $retailpricepct1;
  protected $retailprice1;
  protected $retailpricepct2;
  protected $retailprice2;

  /**
   * @var int $lvlapricepct1  The products' primary level A price percentage. Max 2 characters.
   * @var int $levelaprice1  The products' primary level A price amount. Max 6 characters.
   * @var int $lvlaqtybreak1  The products' primary level A quantity break amount. Max 3 characters.
   * @var int $lvlapricepct2  The products' secondary level A price percentage Max 2 characters.
   * @var int $levelaprice2  The products' secondary level A price amount Max 6 characters.
   * @var int $lvlaqtybreak2  The products' secondary level A quantity break amount Max 3 characters.
   */
  protected $lvlapricepct1;
  protected $levelaprice1;
  protected $lvlaqtybreak1;
  protected $lvlapricepct2;
  protected $levelaprice2;
  protected $lvlaqtybreak2;

  /**
   * @var int $lvlbpricepct1  The products' primary level B price percentage. Max 2 characters.
   * @var int $levelbprice1  The products' primary level B price amount. Max 6 characters.
   * @var int $lvlbqtybreak1  The products' primary level B quantity break amount. Max 3 characters.
   * @var int $lvlbpricepct2  The products' secondary level B price percentage Max 2 characters.
   * @var int $levelbprice2  The products' secondary level B price amount Max 6 characters.
   * @var int $lvlbqtybreak2  The products' secondary level B quantity break amount Max 3 characters.
   */
  protected $lvlbpricepct1;
  protected $levelbprice1;
  protected $lvlbqtybreak1;
  protected $lvlbpricepct2;
  protected $levelbprice2;
  protected $lvlbqtybreak2;

  /**
   * @var int $lvlcpricepct1  The products' primary level c price percentage. Max 2 characters.
   * @var int $levelcprice1  The products' primary level c price amount. Max 6 characters.
   * @var int $lvlcqtybreak1  The products' primary level c quantity break amount. Max 3 characters.
   * @var int $lvlcpricepct2  The products' secondary level c price percentage Max 2 characters.
   * @var int $levelcprice2  The products' secondary level c price amount Max 6 characters.
   * @var int $lvlcqtybreak2  The products' secondary level c quantity break amount Max 3 characters.
   */
  protected $lvlcpricepct1;
  protected $levelcprice1;
  protected $lvlcqtybreak1;
  protected $lvlcpricepct2;
  protected $levelcprice2;
  protected $lvlcqtybreak2;

  /**
   * @var int $lvldpricepct1  The products' primary level D price percentage. Max 2 characters.
   * @var int $leveldprice1  The products' primary level D price amount. Max 6 characters.
   * @var int $lvldqtybreak1  The products' primary level D quantity break amount. Max 3 characters.
   * @var int $lvldpricepct2  The products' secondary level D price percentage Max 2 characters.
   * @var int $leveldprice2  The products' secondary level D price amount Max 6 characters.
   * @var int $lvldqtybreak2  The products' secondary level D quantity break amount Max 3 characters.
   */
  protected $lvldpricepct1;
  protected $leveldprice1;
  protected $lvldqtybreak1;
  protected $lvldpricepct2;
  protected $leveldprice2;
  protected $lvldqtybreak2;

  /**
   * @var int $lvlepricepct1  The products' primary level E price percentage. Max 2 characters.
   * @var int $leveleprice1  The products' primary level E price amount. Max 6 characters.
   * @var int $lvleqtybreak1  The products' primary level E quantity break amount. Max 3 characters.
   * @var int $lvlepricepct2  The products' secondary level E price percentage Max 2 characters.
   * @var int $leveleprice2  The products' secondary level E price amount Max 6 characters.
   * @var int $lvleqtybreak2  The products' secondary level E quantity break amount Max 3 characters.
   */
  protected $lvlepricepct1;
  protected $leveleprice1;
  protected $lvleqtybreak1;
  protected $lvlepricepct2;
  protected $leveleprice2;
  protected $lvleqtybreak2;

  /**
   * @var string $datecreated The date this item was created. Max 6 characters.
   * @var string $timecreated The timestamp of this item when it was created. Max 4 characters.
   * @var string $createdby The user that this item was created by. Max 4 characters.
   */
  protected $datecreated;
  protected $timecreated;
  protected $createdby;

  /**
   * @var string $dateupdated The date this item was updated. Max 6 characters.
   * @var string $timeupdated The timestamp of this item when it was updated. Max 4 characters.
   * @var string $updatedby The user that this item was updated by. Max 4 characters.
   */
  protected $dateupdated;
  protected $timeupdated;
  protected $updatedby;

  /**
   * @var string $protected __Unkown use__. Max 1 characters.
   * @var string $protecteddetail __Unkown use__. Max 1 characters.
   * @var int $protectedmonths __Unkown use__. Max 6 characters.
   */
  protected $protected;
  protected $protecteddetail;
  protected $protectedmonths;

  /**
   * @var string $lc_filler __Unkown use__. Max 133 characters.
   */
  protected $lc_filler;
}
