<?php

namespace Silk\Commands;

use Silk\{ Config\Registry, Console\Command, Utils };
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\{ Style\SymfonyStyle, Helper\ProgressBar };

class ProductUpdateAttributeSetCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("product:update:attribute-set")

      ->setAliases(array("product:update:attrset"))

      ->setDescription("Update the attribute set for one or more Magento products")

      ->setHelp("- Using this command to change the attribute sets for your products in bulk is where it shines.\n  The following example will update the attribute set to <comment>29</comment> of every product thats' SKU starts\n  with <comment>MIT</comment> and has <comment>battery</comment> anywhere in it's name.\n\n  <fg=green;options=bold>$</> <info>php %command.full_name% MIT% %battery% 29</info>")

      ->addArgument("sku", InputArgument::REQUIRED, "The SKU of the product(s) to be queried for")
      ->addArgument("name", InputArgument::REQUIRED, "The name of the product(s) to be queried for")
      ->addArgument("attr", InputArgument::REQUIRED, "The attribute set ID being applyied to all products captured by the query")

      ->addOption("status", "s", InputOption::VALUE_REQUIRED, "Limit query results with product status option <comment>[values: 0, 1, 2]</comment>\n<comment>0</comment>: all\n<comment>1</comment>: enabled\n<comment>2</comment>: disabled", 2)
      ->addOption("exclude", "e", InputOption::VALUE_REQUIRED, "Exclude products from the query by name", "")
      ->addOption("supersearch", "S", InputOption::VALUE_NONE, "<fg=red;options=bold>DOA</> When this option is passed, the name parameter is searched for anywhere in the product names")
      ;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    # Boilerplate
    $io = new SymfonyStyle($input, $output);
    $services = Registry::load("services");
    $connections = Registry::load("connections");

    # Basic logger for now
    # TODO: fix up a better stream handler for the product commands
    $log = $services->get("Logger")
      ->withName("Product")
      ->pushHandler($services->get("StreamHandler\Debug"))
      ;

    /*

    $opts = array(
      'status' => $input->getOption('status') > 0 ? $input->getOption('status') : "%",
      'exclude' => mb_strtoupper("{$input->getOption('exclude')}"),
      'supersearch' => $input->getOption('supersearch'),
    );

    $args = array(
      'sku' => mb_strtoupper($input->getArgument('sku')).'%',
      'name' => mb_strtoupper(($opts['supersearch'] == 0 ? "{$input->getArgument('name')}%" : "%{$input->getArgument('name')}%")),
      'attr' => (int)$input->getArgument('attr'),
    );

    */

    if ($io->isVerbose()) $io->section("Connecting to clients...");

    # Attempt a connection to the Magento 2 database
    $message = "Connecting to {$connections['mage']['host']}:{$connections['mage']['port']} ...";
    $log->notice($message);
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $mageDb = Utils::connect_database($connections["mage"], $io);

    # Bind any and all needed parameters to our prepared statement
    # Finally, execute the query statement
    $sql = file_get_contents(Registry::load("sql_directory")."/mage_search_product_to_edit.sql");
    $statement = $mageDb->prepare($sql);
    $statement->bindValue(":status", $input->getOption("status"), \PDO::PARAM_STR);
    $statement->bindValue(":sku", $input->getArgument("sku"), \PDO::PARAM_STR);
    $statement->bindValue(":name", $input->getArgument("name"), \PDO::PARAM_STR);
    $statement->bindValue(":exclude", $input->getOption("exclude"), \PDO::PARAM_STR);
    $statement->execute();

    # If there are no products to be imported, kill the script
    if (!Utils::has_results($statement)):
      // var_dump($statement->rowCount());
      $message = "No results found for query.";
      $log->warning($message);
      if (isset($monitor)) $monitor->complete($message);
      die($io->text("<fg=red>{$message}</>"));
    endif;

    # Fetch the Magento API client from our application DI container
    $api = $services->get("Mage\Api\Client");

    if ($io->isVerbose()):
      $io->section("Fetching database results, executing queries and posting requests...");
      $progress = new ProgressBar($output, $statement->rowCount());
      $progress->setOverwrite(true);
      $progress->setFormat('debug');
      $progress->start();
    endif;

    # Set the update counter to 0
    # Loop through query results and perform our product updates
    $counter = 0;
    while ($product = $statement->fetch(\PDO::FETCH_OBJ)):
      # Increment our counter for passing over a product
      $counter++;

      if ($io->isVerbose()):
        $progress->clear();
        $io->text("<fg=blue>{$product->sku}:</> Changing attribute set...");
        $progress->display();
      endif;

      $request = $api->updateProduct($product->sku, [
        'product' => [
          'status' => $product->status,
          'sku' => $product->sku,
          'attribute_set_id' => $input->getArgument("attr")
        ],
        'save_options' => true,
      ]);

      $log->info("{$product->sku}: [OK] {$request->getStatusCode()}");
      if ($io->isVerbose()):
        $progress->clear();
        $io->text("<info>[OK] {$request->getStatusCode()}</info>");
        $progress->display();
        $progress->advance();
      endif;
    endwhile;

    if ($io->isVerbose()):
      $progress->clear();
      $io->section('Disconnecting from clients...');
    endif;

    $message = "Disconnected from {$mageDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;

    $mageDb = $statement = $product = null;

    # Kill API client connections as well as nullify any potentially sensitive vars
    $api->kill();

    # Finish progress bar and display results to the user
    if ($io->isVerbose()):
      $io->section('Printing results...');
      $progress->finish();
    endif;

    $log->info("Updated the attribute set for {$counter} product(s).");
    # $log->info("Missed {$counterErr} product(s) due to errors. Details below");
    if ($io->isVerbose()):
      $io->newLine(2);
      $io->success([
        "[RESULTS]",
        "Updated the attribute set for {$counter} product(s)",
        # "Missed {$counterErr} product(s) due to errors. Details below",
      ]);
    endif;
  }
}
