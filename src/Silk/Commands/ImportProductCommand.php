<?php

namespace Silk\Commands;

use Silk\{ Config\Registry, Console\Command, Utils };
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\{ Style\SymfonyStyle, Helper\ProgressBar };

class ImportProductCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("import:product")

      ->setAliases(array("import:sku", "import:part"))

      ->setDescription("Import one or more products from Silk")

      ->setHelp("All options for querying use MySQL valid syntax (e.g., % = wildcard).")

      ->addOption("sku", "s", InputOption::VALUE_OPTIONAL, "Product part number (SKU) to run query on.", "%")
      ->addOption("cat", "c", InputOption::VALUE_OPTIONAL, "Product category (CAT) to run query on.", "%")
      ->addOption("div", "d", InputOption::VALUE_OPTIONAL, "Product division (DIV) to run query on.", "%")
      ;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    $io = new SymfonyStyle($input, $output);
    $services = Registry::load("services");
    $connections = Registry::load("connections");

    $log = $services->get("Logger")
      ->withName("Import")
      ->pushHandler($services->get("StreamHandler\Debug"))
      ;

    if ($io->isVerbose()) $io->section("Connecting to clients ...");

    # Attempt a connection to the Silk database
    $message = "Connecting to {$connections['silk']['host']}:{$connections['silk']['port']} ...";
    $log->notice("{$message}");
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $silkDb = Utils::connect_database($connections["silk"], $io);

    # Bind any and all needed parameters to our prepared statement
    # Finally, execute the query statement
    $silkQuery = file_get_contents(Registry::load("sql_directory")."/all_items.sql");
    $silkStatement = $silkDb->prepare($silkQuery);
    $silkStatement->bindValue(":cat", $input->getOption("cat"), \PDO::PARAM_STR);
    $silkStatement->bindValue(":div", $input->getOption("div"), \PDO::PARAM_STR);
    $silkStatement->bindValue(":sku", $input->getOption("sku"), \PDO::PARAM_STR);
    $silkStatement->execute();

    # If there are no products to be imported, kill the script
    if (!Utils::has_results($silkStatement)):
      $message = "No results found for query.";
      $log->warning($message);
      if (isset($monitor)) $monitor->complete($message);
      die($io->text("<fg=red>{$message}</>"));
    endif;

    # Attempt a connection to the Magento 2 database
    $message = "Connecting to {$connections['mage']['host']}:{$connections['mage']['port']} ...";
    $log->notice($message);
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $mageDb = Utils::connect_database($connections["mage"], $io);

    # Fetch the Magento API client from our application DI container
    $api = $services->get("Mage\Api\Client");

    if ($io->isVerbose()):
      $io->section('Fetching database results, executing queries and posting requests...');
      $progress = new ProgressBar($output, $silkStatement->rowCount());
      $progress->setOverwrite(true);
      $progress->setFormat('debug');
      $progress->start();
    endif;

    # Set new and exisitng product counters to 0
    # Loop through the Silk qury statement results and handle each accordingly
    $newProducts = 0;
    $existingProducts = 0;
    while ($silkProduct = $silkStatement->fetch(\PDO::FETCH_OBJ)):
      $mageQuery = "SELECT entity_id AS id FROM catalog_product_entity WHERE entity_id = {$silkProduct->id}";
      $mageStatement = $mageDb->prepare($mageQuery);
      $mageStatement->execute();

      if ($io->isVerbose()):
        $progress->clear();
        $io->text("<fg=blue>{$silkProduct->sku}</>:");
        $progress->display();
      endif;

      # Check for results
      if (Utils::has_results($mageStatement)):
        # Increment the existing product counter since the product already exists in Magento
        $existingProducts++;

        $log->info("{$silkProduct->sku}: Already in catalog... skipping...");
        if ($io->isVerbose()):
          $progress->clear();
          $io->text("Already in catalog... Skipping...");
          $progress->display();
          $progress->advance();
        endif;
      else:
        # Increment the new product counter since it has not been found in the Mage database
        $newProducts++;

        $log->info("{$silkProduct->sku}: Not in catalog... Adding product...");
        if ($io->isVerbose()):
          $progress->clear();
          $io->text("Not in catalog... Adding product...");
          $progress->display();
        endif;

        # Normalize the decimal quantities from the Silk DB.
        $silkFixedQty = Utils::silk_math($silkProduct->qty, '*', $silkProduct->u_m);

        # Build JSON request to send via REST API.
        $request = $api->addProduct([
          'product' => [
            'status' => 2,
            'type_id' => 'simple',
            'id' => $silkProduct->id,
            'sku' => str_replace('/', '-', $silkProduct->sku), // replace slashes with hyphens in SKUs
            'name' => $silkProduct->name,
            'price' => Utils::silk_math($silkProduct->price_c, '/', $silkProduct->u_m),
            'extension_attributes' => [
              'stock_item' => [
                'qty' => $silkFixedQty,
                'is_in_stock' => Utils::is_in_stock($silkFixedQty),
              ],
            ],
            'weight' => 1,
            'visibility' => 4,
            'attribute_set_id' => 29,
            'tier_prices' => [
              [ // Regular
                'customer_group_id' => 9,
                'qty' => 1,
                'value' => Utils::silk_math($silkProduct->price, '/', $silkProduct->u_m),
              ],
              [ // Level A
                'customer_group_id' => 4,
                'qty' => 1,
                'value' => Utils::silk_math($silkProduct->price_a, '/', $silkProduct->u_m)
              ],
              [ // Level B
                'customer_group_id' => 5,
                'qty' => 1,
                'value' => Utils::silk_math($silkProduct->price_b, '/', $silkProduct->u_m)
              ],
              [ // Level C
                'customer_group_id' => 6,
                'qty' => 1,
                'value' => Utils::silk_math($silkProduct->price_c, '/', $silkProduct->u_m)
              ],
              [ // Level D
                'customer_group_id' => 7,
                'qty' => 1,
                'value' => Utils::silk_math($silkProduct->price_d, '/', $silkProduct->u_m)
              ],
              [ // Level E
                'customer_group_id' => 8,
                'qty' => 1,
                'value' => Utils::silk_math($silkProduct->price_e, '/', $silkProduct->u_m)
              ],
            ],
            'custom_attributes' => [
              [
                'attribute_code' => 'url_key',
                'value' => preg_replace('/\W+/', '-', mb_strtolower($silkProduct->name . '-' . $silkProduct->sku)),
              ],
              [
                'attribute_code' => 'category_ids',
                'value' => [
                  '2', // All
                ],
              ],
            ],
          ],
          'save_options' => true,
        ]);

        $log->info("{$silkProduct->sku}: [OK] {$request->getStatusCode()}");
        if ($io->isVerbose()):
          $progress->clear();
          $io->text("<info>[OK] {$request->getStatusCode()}</info>");
          $progress->display();
          $progress->advance();
        endif;
      endif;
    endwhile;

    if ($io->isVerbose()):
      $progress->clear();
      $io->section('Disconnecting from clients...');
    endif;

    # Database client garbage collection
    $message = "Disconnected from {$silkDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;
    $silkDb = $silkStatement = $silkProduct = null;

    $message = "Disconnected from {$mageDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;
    $mageDb = $mageStatement = $mageProduct = null;

    # REST API client garbage collection
    $api->kill();
    $request = $requestHeaders = $requestBody = $response = $responseBody = null;

    # Finish progress bar and display results to the user
    if ($io->isVerbose()):
      $io->section('Printing results...');
      $progress->finish();
    endif;

    $log->info("Imported {$newProducts} product(s) from Silk");
    $log->info("Skipped {$existingProducts} existing product(s)");
    if ($io->isVerbose()):
      $io->newLine(2);
      $io->success([
        "[RESULTS]",
        "Imported {$newProducts} product(s) from Silk",
        "Skipped {$existingProducts} existing product(s)",
      ]);
    endif;
  }
}
