<?php

namespace Silk\Commands;

use Silk\{ Config\Registry, Console\Command };
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConfigurationDumpCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("configuration:dump")

      ->setAliases(array("config:dump"))

      ->setDescription("Print the current application configuration")

      ->setHelp("- Print out a JSON encoded structure of the current application configuration like so,\n\n  <fg=green;options=bold>$</> <info>php %command.full_name%</info>")

      ;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    # Styled console IO
    $io = new SymfonyStyle($input, $output);

    # Load the Logger and a StreamHandler from the application Registry
    $log = Registry::load("services")->get("Logger")
      ->withName("Configuration")
      ->pushHandler(Registry::load("services")->get("StreamHandler\Debug"))
      ;

    $log->info("Configuration was dumped to console by", [get_current_user()]);
    $io->text(stripslashes(json_encode(Registry::output(), JSON_PRETTY_PRINT)));
  }
}
