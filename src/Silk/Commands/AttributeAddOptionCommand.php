<?php

namespace Silk\Commands;

use Silk\{ Config\Registry, Console\Command, Utils};
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\{ Style\SymfonyStyle, Helper\ProgressBar, Question\Question };

class AttributeAddOptionCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("attribute:add:option")

      ->setAliases(array("attribute:add:opt", "attr:add:option", "attr:add:opt"))

      ->setDescription("Add an option to an existing product attribute.")

      ->setHelp("- Add a new option to an attribute set with options like so,\n\n  <fg=green;options=bold>$</> <info>php %command.full_name% length 1-1/4\\\"")

      ->addArgument("attribute", InputArgument::REQUIRED, "The product attribute being added to.")
      ->addArgument("label", InputArgument::REQUIRED, "The attribute label that will be applied to the new attribute value.")
      ;
  }

  protected function interact (InputInterface $input, OutputInterface $output)
  {
    if (!$input->getArgument("attribute"))
      $input->setArgument(
        "attribute",
        $this->getHelper("question")->ask(
          $input,
          $output,
          new Question(
            "<comment>Please input the ID of the attribute that will be added to:</comment> ",
            null
          )
        )
      );

    if (!$input->getArgument("label"))
      $input->setArgument(
        "label",
        $this->getHelper("question")->ask(
          $input,
          $output,
          new Question(
            "<comment>Please input the label that will be assigned to the new attribute options:</comment> ",
            null
          )
        )
      );
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    # Boilerplate
    $io = new SymfonyStyle($input, $output);
    $services = Registry::load("services");
    $log = $services->get("Logger")
      ->withName("Magento_Attributes")
      ->pushHandler($services->get("StreamHandler\Debug"))
      ;

    $log->info("{$input->getArgument('attribute')}: Added option {$input->getArgument('label')}");
    $io->text("<fg=blue>{$input->getArgument('attribute')}:</> Adding option <info>{$input->getArgument('label')}</info>");

    # Connect to API client and POST our data
    $api = $services->get("Mage\Api\Client");
    $request = $api->addAttributeOption($input->getArgument("attribute"), [
      'option' => [
        'label' => $input->getArgument("label"),
        'is_default' => false,
      ],
    ]);

    $log->info("{$input->getArgument('attribute')}: [OK] {$request->getStatusCode()}");
    $io->text("<info>[OK] {$request->getStatusCode()}</info>");

    # Cleanup
    $api->kill();
  }
}
