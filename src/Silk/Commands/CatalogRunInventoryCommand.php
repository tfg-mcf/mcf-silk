<?php

namespace Silk\Commands;

use Silk\{ Config\Registry, Console\Command, Utils };
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\{ Style\SymfonyStyle, Helper\ProgressBar, Question\ChoiceQuestion };

class CatalogRunInventoryCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("catalog:run:inventory")

      ->setAliases(array("catalog:run:inv", "cat:run:inv"))

      ->setDescription("Sync product stock levels from Anzio Silk to Magento 2.")

      ->setHelp("- The <info>%command.name%</info> command is best run on a cron job schedule like so,\n\n  <fg=blue>*/5 */5 * * *</> <info>/usr/bin/php -f %command.full_name% cycle --quiet</info>\n\n- You can also run this command manually and specify the product category, division, and SKU like so,\n\n  <fg=green;options=bold>$</> <info>php %command.full_name% full --cat=PT --div=MIT --sku=MIT2%</info>")

      ->addArgument(
        "count",
        InputArgument::REQUIRED,
        "The inventory count method that will be executed on the product catalog <comment>[values: \"full\", \"cycle\"]</comment>\n<comment>full</comment>: update stock levels on <options=bold>all</> products\n<comment>cycle</comment>: update stock levels on the current set of updated products (16 min window per product)"
      )

      ->addOption("cron", "C", InputOption::VALUE_NONE, "Is this command being run on a cron job?")
      ->addOption("sku", "s", InputOption::VALUE_OPTIONAL, "A full or partial product SKU that helps to determine which product(s) is/are searched for", "%")
      ->addOption("cat", "c", InputOption::VALUE_OPTIONAL, "A 2 character Silk category code that limits the search for product(s) to the specified category.", "%")
      ->addOption("div", "d", InputOption::VALUE_OPTIONAL, "A 3 character Silk division code that limits the search for product(s) to the specified division.", "%")
      ;
  }

  protected function interact (InputInterface $input, OutputInterface $output)
  {
    # Conditionally handle the "count" argument
    switch ($input->getArgument("count")):
      case "cycle":
      case "full":
        return; # Continue regular command execution for valid cases
        break;
      case null: # Ask the user to select a count type if one was not specified
        $input->setArgument(
          "count",
          $this->getHelper("question")->ask(
            $input,
            $output,
            new ChoiceQuestion(
              "<comment>Please select the inventory method to be executed:</comment> ",
              array("cycle", "full"),
              0 # the default option index to be used if none is specified
            )
          )
        );
        break;
      default: # If any other value is passed, throw an exception to the user
        throw new \RuntimeException("Unknown inventory method `{$input->getArgument('count')}` was entered");
    endswitch;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    # Boilerplate
    $services = Registry::load("services");
    $connections = Registry::load("connections");
    $io = new SymfonyStyle($input, $output);

    # Set up a basic logger
    $log = $services->get("Logger")
      ->withName("Inventory")
      ->pushHandler($services->get("StreamHandler\Stock"))
      ;

    # Set up and start the heartbeat monitor if we are running in cron
    $monitor = !$input->getOption("cron") ? null : $services->get("HeartbeatMonitor\Inventory");
    if ($monitor):
      $log->debug("Started cron job monitor");
      if ($io->isDebug()) $io->text("Started cron job monitor");
      $monitor->run("Syncing stock levels from SILK");
    endif;

    if ($io->isVerbose()) $io->section('Connecting to clients...');

    # Attempt a connection to the Silk database
    $message = "Connecting to {$connections['silk']['host']}:{$connections['silk']['port']} ...";
    $log->notice("{$message}");
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $silkDb = Utils::connect_database($connections["silk"], $io, $monitor);

    # Determine and prepare which query to use based on the count type
    $silkSql = $input->getArgument("count") != "cycle" ? "all_items.sql" : "update_qtys.sql";
    $silkQuery = file_get_contents(Registry::load("sql_directory")."/{$silkSql}");
    $silkStatement = $silkDb->prepare($silkQuery);

    # Bind any and all needed parameters to our prepared statement
    # Finally, execute the query statement
    $silkStatement->bindValue(":cat", $input->getOption("cat"), \PDO::PARAM_STR);
    $silkStatement->bindValue(":div", $input->getOption("div"), \PDO::PARAM_STR);
    $silkStatement->bindValue(":sku", $input->getOption("sku"), \PDO::PARAM_STR);
    $silkStatement->execute();

    # If there are no products to be updated, kill the script
    if (!Utils::has_results($silkStatement)):
      $message = "No results found for query.";
      $log->warning($message);
      if ($monitor) $monitor->complete($message);
      die($io->text("<fg=red>{$message}</>"));
    endif;

    # Attempt a connection to the Magento 2 database
    $message = "Connecting to {$connections['mage']['host']}:{$connections['mage']['port']} ...";
    $log->notice($message);
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $mageDb = Utils::connect_database($connections["mage"], $io, $monitor);

    # Fetch the Magento API client from our application DI container
    $api = $services->get("Mage\Api\Client");
    $io->text($api->getToken());

    if ($io->isVerbose()):
      $io->section('Fetching database results, executing queries and posting requests...');
      $progress = new ProgressBar($output, $silkStatement->rowCount());
      $progress->setOverwrite(true);
      $progress->setFormat('debug');
      $progress->start();
    endif;

    # Set NoUpdate and ToUpdate counters to 0;
    # Loop through the Silk query statement results and handle each accordingly
    $counterNoUpdate = 0;
    $counterToUpdate = 0;
    while ($silkProduct = $silkStatement->fetch(\PDO::FETCH_OBJ)):
      # Prepare the Magento query statement, binding to it, the Silk product ID to confirm a product is online
      # Finally execute the query and fetch the results
      $mageStatement = $mageDb->prepare(file_get_contents(Registry::load("sql_directory")."/update_check.sql"));
      $mageStatement->bindParam(':id', $silkProduct->id, \PDO::PARAM_INT);
      $mageStatement->execute();
      $mageProduct = $mageStatement->fetch(\PDO::FETCH_OBJ);

      if ($io->isVerbose()):
        $progress->clear();
        $io->text("<fg=blue>{$silkProduct->sku}</>:");
        $progress->display();
      endif;

      # Check if Magento query statement has any results
      if (Utils::has_results($mageStatement)):
        if ($io->isVerbose()):
          $progress->clear();
          $io->text("Match found!");
          $progress->display();
        endif;

        # Normalize/fix the Silk & Magento quantity values, storing each to a variable
        # Finally, check if the currently matched product needs a stock update
        $mageFixedQty = Utils::rtrim_zeroes($mageProduct->qty);
        $silkFixedQty = Utils::silk_math($silkProduct->qty, '*', $silkProduct->u_m);
        if ($mageFixedQty == $silkFixedQty):
          # Increment the NoUpdate counter due to stock levels not changing
          $counterNoUpdate++;

          $log->info("{$silkProduct->sku}: Keeping current stock level at {$mageFixedQty}");
          if ($io->isVerbose()):
            $progress->clear();
            $io->text("Keeping current stock level at <comment>{$mageFixedQty}</comment>");
            $io->newLine();
            $progress->display();
            $progress->advance();
          endif;
        else:
          # Increment ToUpdate counter due to stock levels changing
          $counterToUpdate++;

          $log->info("{$silkProduct->sku}: Updating stock levels from {$mageFixedQty} to {$silkFixedQty}");
          if ($io->isVerbose()):
            $progress->clear();
            $io->text("Updating stock levels from <fg=red>{$mageFixedQty}</> to <fg=green>{$silkFixedQty}</>");
            $progress->display();
          endif;

          # Finally, we actaully get to update the products' stock levels
          $request = $api->updateProduct($mageProduct->sku, [
            "product" => [
              "status" => $mageProduct->status,
              "sku" => $mageProduct->sku,
              "extension_attributes" => [
                "stock_item" => [
                  "qty" => $silkFixedQty,
                  "is_in_stock" => Utils::is_in_stock($silkFixedQty),
                ],
              ],
            ],
            "save_options" => true,
          ]);

          $log->info("{$silkProduct->sku}: [OK] {$request->getStatusCode()}");
          if ($io->isVerbose()):
            $progress->clear();
            if ($io->isDebug()) $io->text("<info>[OK] {$request->getStatusCode()}</info> ");
            $io->newLine();
            $progress->display();
            $progress->advance();
          endif;
        endif;
      else:
        # Increment the NoUpdate counter due to the product missing from the catalog
        $counterNoUpdate++;

        $log->warning("{$silkProduct->sku}: No match found! Not in catalog, skipped");
        if ($io->isVerbose()):
          $progress->clear();
          $io->text(['No match found!', 'Not in catalog, skipping ']);
          $io->newLine();
          $progress->display();
          $progress->advance();
        endif;
      endif;
    endwhile;

    if ($io->isVerbose()):
      $progress->clear();
      $io->section('Disconnecting from clients...');
    endif;

    # Database client garbage collection
    $message = "Disconnected from {$silkDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;
    $silkDb = $silkStatement = $silkProduct = null;

    $message = "Disconnected from {$mageDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;
    $mageDb = $mageStatement = $mageProduct = null;

    # REST API client garbage collection
    $api->kill();
    $request = $requestHeaders = $requestBody = $response = $responseBody = null;

    # Finish progress bar and display results to the user
    if ($io->isVerbose()):
      $io->section('Printing results...');
      $progress->finish();
    endif;

    $log->info("Updated stock levels for {$counterToUpdate} product(s)");
    $log->info("Skipped {$counterNoUpdate} product(s)");
    if ($io->isVerbose()):
      $io->newLine(2);
      $io->success([
        "[RESULTS]",
        "Updated stock levels for {$counterToUpdate} product(s)",
        "Skipped {$counterNoUpdate} product(s)"
      ]);
    endif;

    # If we are running as a cron job, end the heartbeat monitor
    if ($monitor):
      $log->debug("Stopped cron job monitor");
      if ($io->isDebug()) $io->text("Stopped cron job monitor");
      $monitor->complete("\nUpdated {$counterToUpdate} product(s)\nSkipped {$counterNoUpdate} product(s)\nView logfile at: ".realpath($log->getHandlers()[0]->getUrl()));
    endif;
  }
}
