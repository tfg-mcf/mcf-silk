<?php

namespace Silk\Commands;

use Mage\Product\Product as Product;
use Silk\{ Config\Registry, Console\Command, Utils };
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\{ Style\SymfonyStyle, Helper\ProgressBar };
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\{ CsvEncoder, JsonEncoder };

class ProductUpdateViaCommand extends Command
{
  protected function configure()
  {
    $this
      ->setName('product:update:via')

      ->setAliases(array("product:update:from"))

      ->setDescription('Update products via an external data file')

      ->setHelp('This command updates products from an exertnal data source file')

      ->addArgument('source', InputArgument::REQUIRED, 'The source of the data file')
      ->addArgument('sku', InputArgument::REQUIRED, 'The product SKU query string parameter')
      ->addArgument('name', InputArgument::REQUIRED, 'The product name query string parameter')

      ->addOption('exclude', 'e', InputOption::VALUE_REQUIRED, 'Products to exlude by name', "")
      ->addOption("status", "S", InputOption::VALUE_REQUIRED, "Products to include within query results by status.\n(0=all, 1=enabled, 2=disabled)", 2)
      ->addOption("pre_len", "p", InputOption::VALUE_REQUIRED, "Length of vendor prefix to get from SKU argument value.", 3)

      # NOTE: Need to figure out a way to get the value of an attribute_code based on a label
      ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    # Boilerplate
    $io = new SymfonyStyle($input, $output);
    $services = Registry::load("services");
    $connections = Registry::load("connections");

    # Set up a basic logger
    $log = $services->get("Logger")
      ->withName("Product")
      ->pushHandler($services->get("StreamHandler\Debug"))
      ;

    /*

    $args = array(
      "sku" => mb_strtoupper($input->getArgument("sku")),
      "name" => mb_strtoupper($input->getArgument("name")),
      "source" => realpath(__DIR__."/../../../data/{$input->getArgument("source")}"),
    );

    $opts = array(
      'status' => $input->getOption('status') > 0 ? $input->getOption('status') : "%",
      'exclude' => mb_strtoupper("{$input->getOption('exclude')}"),
      "pre_len" => $input->getOption("pre_len")
    );

    */

    define("SOURCE", realpath(Registry::load("data_directory")."/product/{$input->getArgument("source")}"));

    if ($io->isVerbose()) $io->section("Connecting to clients...");

    # Attempt a connection to the Magento 2 database
    $message = "Connecting to {$connections['mage']['host']}:{$connections['mage']['port']} ...";
    $log->notice($message);
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $mageDb = Utils::connect_database($connections["mage"], $io);

    # Bind any and all needed parameters to our prepared statement
    # Finally, execute the query statement
    $sql = file_get_contents(Registry::load("sql_directory")."/mage_search_product_to_edit.sql");
    $statement = $mageDb->prepare($sql);
    $statement->bindValue(":status", $input->getOption("status"), \PDO::PARAM_STR);
    $statement->bindValue(":sku", $input->getArgument("sku"), \PDO::PARAM_STR);
    $statement->bindValue(":name", $input->getArgument("name"), \PDO::PARAM_STR);
    $statement->bindValue(":exclude", $input->getOption("exclude"), \PDO::PARAM_STR);
    $statement->execute();

    # If there are no products to be imported, kill the script
    if (!Utils::has_results($statement)):
      // var_dump($statement->rowCount());
      $message = "No results found for query.";
      $log->warning($message);
      if (isset($monitor)) $monitor->complete($message);
      die($io->text("<fg=red>{$message}</>"));
    endif;

    # Fetch the Magento API client from our application DI container
    $api = $services->get("Mage\Api\Client");

    $serializer = new Serializer(
      array(new ObjectNormalizer),
      array(new CsvEncoder, new JsonEncoder)
    );

    // Decode CSV data source file
    $csv = $serializer->decode(
      file_get_contents(SOURCE),
      pathinfo(SOURCE, PATHINFO_EXTENSION)
    );

    define("VENDOR_PREFIX", mb_substr($input->getArgument("sku"), 0, $input->getOption("pre_len")));

    // Convert CSV to JSON
    $products = array();
    foreach ($csv as $row):
      $product = $serializer->deserialize(json_encode($row), "Mage\Product\Product", 'json', [
        'allow_extra_attributes' => true
      ]);
      $products[VENDOR_PREFIX.$product->getSku()] = $product;
    endforeach;

    if ($io->isVerbose()):
      $io->section("Fetching database results, executing queries and posting requests...");
      $progress = new ProgressBar($output, $statement->rowCount());
      $progress->setOverwrite(true);
      $progress->setFormat('debug');
      $progress->start();
    endif;

    // // Set data from mage db
    // foreach ($products as $product):
    //   $product->setStatus(2); // SQL
    //   $product->setSku(VENDOR_PREFIX.$product->getSku()); // SQL
    //   // $io->text(json_encode($product, JSON_PRETTY_PRINT));
    // endforeach;

    # Set the counter to 0
    # Loop through database query results
    $counter = 0;
    while ($product = $statement->fetch(\PDO::FETCH_OBJ)):
      if (!array_key_exists($product->sku, $products)):
        $log->info(VENDOR_PREFIX.current($products)->getSku()." No product in database");
        if ($io->isVerbose()):
          $progress->clear();
          $io->text(VENDOR_PREFIX.current($products)->getSku()." No product in database");
          $progress->display();
        endif;
      else:
        # Increment the counter for passing over a found product
        $counter++;

        $log->info("{$product->sku}: Updating product data from ".SOURCE);
        if ($io->isVerbose()):
          $progress->clear();
          $io->text("<fg=blue>{$product->sku}:</> Updating product data from ".SOURCE);
          $progress->display();
        endif;

        $products[$product->sku]->setStatus($product->status);
        $products[$product->sku]->setSku($product->sku);

        $request = $api->updateProduct($product->sku, (array) $products[$product->sku]);

        if ($io->isDebug()):
          $progress->clear();
          $io->text("<comment>".json_encode($products[$product->sku], JSON_PRETTY_PRINT)."</comment>");
          $progress->display();
        endif;
      endif;

      $log->info("{$product->sku}: [OK] {$request->getStatusCode()}");
      if ($io->isVerbose()):
        $progress->clear();
        $io->text("<info>[OK] {$request->getStatusCode()}</info>");
        $progress->display();
        $progress->advance();
      endif;
    endwhile;

    if ($io->isVerbose()):
      $progress->clear();
      $io->section('Disconnecting from clients...');
    endif;

    # Database garbage collection
    $message = "Disconnected from {$mageDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;

    $mageDb = $statement = $product = null;

    # Kill API client connections as well as nullify any potentially sensitive vars
    $api->kill();

    # Finish progress bar and display results to the user
    if ($io->isVerbose()):
      $io->section('Printing results...');
      $progress->finish();
    endif;

    $log->info("Updated data for {$counter} product(s).");
    // $log->info("Missed {$counterErr} product(s) due to errors. Details below");
    if ($io->isVerbose()):
      $io->newLine(2);
      $io->success([
        "[RESULTS]",
        "Updated data for {$counter} product(s)",
        // "Missed {$counterErr} product(s) due to errors. Details below",
      ]);
    endif;
  }
}
