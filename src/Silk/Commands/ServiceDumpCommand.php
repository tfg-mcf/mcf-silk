<?php

namespace Silk\Commands;

use Silk\{Config\Registry, Console\Command, Utils};
use Symfony\Component\Console\Input\{InputInterface, InputArgument, InputOption};
use Symfony\Component\Console\Output\{OutputInterface};
use Symfony\Component\Console\Style\SymfonyStyle;

class ServiceDumpCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName("service:dump")
      ->setDescription("Print the current set of registered application services")
      ->setHelp("N/A")
      ;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    $io = new SymfonyStyle($input, $output);
    $services = Registry::load("services");

    $log = $services->get("Logger")
      ->withName("Services")
      ->pushHandler($services->get("Silk\Logger\Handler\Debug"))
      ;

    $log->info("Configuration was dumped to console by", [get_current_user()]);
    $io->text(stripslashes(json_encode($services->getServiceIds(), JSON_PRETTY_PRINT)));
  }
}
