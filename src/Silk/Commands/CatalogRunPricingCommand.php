<?php

namespace Silk\Commands;

use Silk\{ Config\Registry, Console\Command, Utils };
use Symfony\Component\Console\Input\{ InputInterface, InputArgument, InputOption };
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\{ Style\SymfonyStyle, Helper\ProgressBar, Question\ChoiceQuestion };

class CatalogRunPricingCommand extends Command
{
  protected function configure ()
  {
    $this
      ->setName('catalog:run:pricing')

      ->setAliases(array("catalog:run:prices"))

      ->setDescription('Sync product level pricing from Anzio Silk to Magento 2.')

      ->setHelp("- The <info>%command.name%</info> command is best run on a cron job schedule like so,\n\n  <fg=blue>*/5 */5 * * *</> <info>/usr/bin/php -f %command.full_name% cycle --quiet</info>\n\n- You can also run this command manually and specify the product category, division, and SKU like so,\n\n  <fg=green;options=bold>$</> <info>php %command.full_name% full --cat=PT --div=MIT --sku=MIT2%</info>\n\n- The <comment>--force</comment> option is handy when you want to force an update on every products' pricing structure, regardless of needing one or not\n\n  <fg=green;options=bold>$</> <info>php %command.full_name% full --force</info>")

      ->addArgument(
        "type",
        InputArgument::REQUIRED,
        "The type of pricing to be executed on the online catalog. <comment>[values: \"full\", \"cycle\"]</comment>\n<comment>full</comment>: run pricing on <options=bold>all</> products\n<comment>cycle</comment>: run pricing on the current set of updated products (16 min window per product)"
      )

      ->addOption("sku", "s", InputOption::VALUE_OPTIONAL, "A full or partial product SKU that helps to determine which product(s) is/are searched for", "%")
      ->addOption("cat", "c", InputOption::VALUE_OPTIONAL, "A 2 character Silk category code that limits the search for product(s) to the specified category.", "%")
      ->addOption("div", "d", InputOption::VALUE_OPTIONAL, "A 3 character Silk division code that limits the search for product(s) to the specified division.", "%")
      ->addOption("force", "F", InputOption::VALUE_NONE, "Force the products to be updated even if one isn't required.")
      ;
  }

  protected function interact (InputInterface $input, OutputInterface $output)
  {
    # Conditionally handle the "type" argument
    switch ($input->getArgument("type")):
      case "cycle":
      case "full":
        return; # Continue regular command execution for valid cases
        break;
      case null: # Ask the user to select a type type if one was not specified
        $input->setArgument(
          "type",
          $this->getHelper("question")->ask(
            $input,
            $output,
            new ChoiceQuestion(
              "<comment>Please select the pricing method to be executed:</comment> ",
              array("cycle", "full"),
              0 # the default option index to be used if none is specified
            )
          )
        );
        break;
      default: # If any other value is passed, throw an exception to the user
        throw new \RuntimeException("Unknown pricing method `{$input->getArgument('type')}` was entered");
    endswitch;
  }

  protected function execute (InputInterface $input, OutputInterface $output)
  {
    # Boilerplate
    $services = Registry::load("services");
    $connections = Registry::load("connections");
    $io = new SymfonyStyle($input, $output);

    # Set up a basic logger
    $log = $services->get("Logger")
      ->withName("Pricing")
      ->pushHandler($services->get("StreamHandler\Debug"))
      ;

    if ($io->isVerbose()) $io->section('Connecting to clients...');

    # Attempt a connection to the Silk database
    $message = "Connecting to {$connections['silk']['host']}:{$connections['silk']['port']} ...";
    $log->notice("{$message}");
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $silkDb = Utils::connect_database($connections["silk"], $io);

    # Determine and prepare which query to use based on the count type
    $silkSql = $input->getArgument("type") != "cycle" ? "all_items.sql" : "update_prices.sql";
    $silkQuery = file_get_contents(Registry::load("sql_directory")."/{$silkSql}");
    $silkStatement = $silkDb->prepare($silkQuery);

    # Bind any and all needed parameters to our prepared statement
    # Finally, execute the query statement
    $silkStatement->bindValue(":cat", $input->getOption("cat"), \PDO::PARAM_STR);
    $silkStatement->bindValue(":div", $input->getOption("div"), \PDO::PARAM_STR);
    $silkStatement->bindValue(":sku", $input->getOption("sku"), \PDO::PARAM_STR);
    $silkStatement->execute();

    # If there are no products to be updated, kill the script
    if (!Utils::has_results($silkStatement)):
      $message = "No results found for query.";
      $log->warning($message);
      if ($monitor) $monitor->complete($message);
      die($io->text("<fg=red>{$message}</>"));
    endif;

    # Attempt a connection to the Magento 2 database
    $message = "Connecting to {$connections['mage']['host']}:{$connections['mage']['port']} ...";
    $log->notice($message);
    if ($io->isVerbose()) $io->text("<comment>{$message}</comment>");
    $mageDb = Utils::connect_database($connections["mage"], $io);

    # Fetch the Magento API client from our application DI container
    $api = $services->get("Mage\Api\Client");

    if ($io->isVerbose()):
      $io->section('Executing queries and requests...');
      $progress = new ProgressBar($output, $silkStatement->rowCount());
      $progress->setOverwrite(true);
      $progress->setFormat('debug');
      $progress->start();
    endif;

    # Set NoUpdate and ToUpdate counters to 0;
    # Loop through the Silk query statement results and handle each accordingly
    $counterNoUpdate = 0;
    $counterToUpdate = 0;
    while ($silkProduct = $silkStatement->fetch(\PDO::FETCH_OBJ)):
      # Prepare the Magento query statement, binding to it, the Silk product ID to confirm a product is online
      # Finally execute the query and fetch the results
      $mageStatement = $mageDb->prepare(file_get_contents(Registry::load("sql_directory")."/update_check.sql"));
      $mageStatement->bindParam(':id', $silkProduct->id, \PDO::PARAM_INT);
      $mageStatement->execute();
      $mageProduct = $mageStatement->fetch(\PDO::FETCH_OBJ);

      if ($io->isVerbose()):
        $progress->clear();
        $io->text("<fg=blue>{$silkProduct->sku}</>:");
        $progress->display();
      endif;

      # Check if Magento query statement has any results
      if (Utils::has_results($mageStatement)):
        if ($io->isVerbose()):
          $progress->clear();
          $io->text("Match found!");
          $progress->display();
        endif;

        # Normalize/fix the Silk & Magento pricing values, storing each in a variable
        # Finally, check if the currently matched product needs a pricing update
        $currentPrice = number_format($mageProduct->price, 2, '.', '');
        // $newPrice = Utils::silk_math($silkProduct->price_c, '/', $silkProduct->u_m);
        if ($currentPrice == Utils::silk_math($silkProduct->price_c, '/', $silkProduct->u_m)
        and !$input->getOption("force")):
          # Increment the NoUpdate counter due to pricing levels not changing
          $counterNoUpdate++;

          $log->info("{$silkProduct->sku}: Keeping current price at {$curentPrice}");
          if ($io->isVerbose()):
            $progress->clear();
            $io->text("Keeping current price at <comment>{$currentPrice}</comment>");
            $io->newLine();
            $progress->display();
            $progress->advance();
          endif;
        else:
          # Increment ToUpdate counter due to stock levels changing
          $counterToUpdate++;

          $log->info("{$silkProduct->sku}: Updated price from {$currentPrice} to ".Utils::silk_math($silkProduct->price_c, "/", $silkProduct->u_m)."...");
          $log->info("{$silkProduct->sku}: Updated LEVEL A price to ".Utils::silk_math($silkProduct->price_a, "/", $silkProduct->u_m)."...");
          $log->info("{$silkProduct->sku}: Updated LEVEL B price to ".Utils::silk_math($silkProduct->price_b, "/", $silkProduct->u_m)."...");
          $log->info("{$silkProduct->sku}: Updated LEVEL C price to ".Utils::silk_math($silkProduct->price_c, "/", $silkProduct->u_m)."...");
          $log->info("{$silkProduct->sku}: Updated LEVEL D price to ".Utils::silk_math($silkProduct->price_d, "/", $silkProduct->u_m)."...");
          $log->info("{$silkProduct->sku}: Updated LEVEL E price to ".Utils::silk_math($silkProduct->price_e, "/", $silkProduct->u_m)."...");
          if ($io->isVerbose()):
            $progress->clear();
            $io->text("{$silkProduct->sku}: Updating price from <fg=red>{$currentPrice}</> to <fg=green>".Utils::silk_math($silkProduct->price_c, "/", $silkProduct->u_m)."</>");
            $progress->display();
          endif;

          # Finally, we get to update the product price levels
          $request = $api->updateProduct($mageProduct->sku, [
            'product' => [
              'status' => $mageProduct->status,
              'sku' => $mageProduct->sku,
              'price' => Utils::silk_math($silkProduct->price_c, '/', $silkProduct->u_m),
              'tier_prices' => [
                [ # Regular
                  'customer_group_id' => 9,
                  'qty' => 1,
                  'value' => Utils::silk_math($silkProduct->price, '/', $silkProduct->u_m),
                ],
                [ # Level A
                  'customer_group_id' => 4,
                  'qty' => 1,
                  'value' => Utils::silk_math($silkProduct->price_a, '/', $silkProduct->u_m)
                ],
                [ # Level B
                  'customer_group_id' => 5,
                  'qty' => 1,
                  'value' => Utils::silk_math($silkProduct->price_b, '/', $silkProduct->u_m)
                ],
                [ # Level C
                  'customer_group_id' => 6,
                  'qty' => 1,
                  'value' => Utils::silk_math($silkProduct->price_c, '/', $silkProduct->u_m)
                ],
                [ # Level D
                  'customer_group_id' => 7,
                  'qty' => 1,
                  'value' => Utils::silk_math($silkProduct->price_d, '/', $silkProduct->u_m)
                ],
                [ # Level E
                  'customer_group_id' => 8,
                  'qty' => 1,
                  'value' => Utils::silk_math($silkProduct->price_e, '/', $silkProduct->u_m)
                ],
              ],
            ],
            'save_options' => true,
          ]);

          $log->info("{$silkProduct->sku}: [OK] {$request->getStatusCode()}");
          if ($io->isVerbose()):
            $progress->clear();
            if ($io->isDebug()) $io->text("<info>[OK] {$request->getStatusCode()}</info>");
            $io->newLine();
            $progress->display();
            $progress->advance();
          endif;
        endif;
      else:
        # Increment the NoUpdate counter due to the product missing from the catalog
        $counterNoUpdate++;

        $log->warning("{$silkProduct->sku}: No match found! Not in catalog, skipped");
        if ($io->isVerbose()):
          $progress->clear();
          $io->text(["No match found!", "Not in catalog, skipped"]);
          $io->newLine();
          $progress->display();
          $progress->advance();
        endif;
      endif;
    endwhile;

    if ($io->isVerbose()):
      $progress->clear();
      $io->section('Disconnecting from clients...');
    endif;

    # Database client garbage collection
    $message = "Disconnected from {$silkDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;
    $silkDb = $silkStatement = $silkProduct = null;

    $message = "Disconnected from {$mageDb->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}";
    $log->notice($message);
    if ($io->isVerbose()):
      $io->text($message);
      $io->text("<fg=red>MySQL server has gone away!</>");
    endif;
    $mageDb = $mageStatement = $mageProduct = null;

    # REST API client garbage collection
    $api->kill();
    $request = $requestHeaders = $requestBody = $response = $responseBody = null;

    # Finish progress bar and display results to the user
    if ($io->isVerbose()):
      $io->section('Printing results...');
      $progress->finish();
    endif;

    $log->info("Updated pricing levels for {$counterToUpdate} product(s)");
    $log->info("Skipped {$counterNoUpdate} product(s)");
    if ($io->isVerbose()):
      $io->newLine(2);
      $io->success([
        "[RESULTS]",
        "Updated pricing levels for {$counterToUpdate} product(s)",
        "Skipped {$counterNoUpdate} product(s)"
      ]);
    endif;
  }
}
