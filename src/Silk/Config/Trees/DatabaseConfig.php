<?php

namespace Silk\Config\Trees;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class DatabaseConfig implements ConfigurationInterface
{
  public function getConfigTreeBuilder ()
  {
    $treeBuilder = new TreeBuilder;

    $rootNode = $treeBuilder->root("database");

    $rootNode
      ->children()
        ->arrayNode("connections")
          ->prototype("array")
            ->children()
              ->scalarNode("host")->isRequired()->cannotBeEmpty()->end()
              ->integerNode("port")->isRequired()->defaultValue(3306)->end()
              ->scalarNode("schema")->isRequired()->cannotBeEmpty()->end()
              ->scalarNode("username")->isRequired()->cannotBeEmpty()->end()
              ->scalarNode("password")->isRequired()->cannotBeEmpty()->end()
            ->end()
          ->end()
        ->end()
      ->end()
      ;

    return $treeBuilder;
  }
}
