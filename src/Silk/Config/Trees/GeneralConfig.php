<?php

namespace Silk\Config\Trees;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class GeneralConfig implements ConfigurationInterface
{
  public function getConfigTreeBuilder ()
  {
    $treeBuilder = new TreeBuilder;

    $rootNode = $treeBuilder->root("general");

    $rootNode
      ->children()
        ->scalarNode("config_directory")->defaultValue(ROOT_DIR."/app/config")->cannotBeEmpty()->end()
        ->scalarNode("data_directory")->defaultValue(ROOT_DIR."/app/data")->cannotBeEmpty()->end()
        ->scalarNode("environment_file")->defaultValue(ROOT_DIR."/app/etc/.env")->cannotBeEmpty()->end()
        ->scalarNode("log_directory")->defaultValue(ROOT_DIR."/var/log")->cannotBeEmpty()->end()
        ->scalarNode("sql_directory")->defaultValue(ROOT_DIR."/app/data/sql")->cannotBeEmpty()->end()
      ->end()
      ;

    return $treeBuilder;
  }
}
