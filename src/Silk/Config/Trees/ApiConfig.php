<?php

namespace Silk\Config\Trees;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class ApiConfig implements ConfigurationInterface
{
  public function getConfigTreeBuilder ()
  {
    $treeBuilder = new TreeBuilder;

    $rootNode = $treeBuilder->root("api");

    $rootNode
      ->children()
        ->arrayNode("clients")
          ->children()
            ->arrayNode("mage")
              ->children()
                ->scalarNode("base_uri")->isRequired()->cannotBeEmpty()->end()
                ->scalarNode("endpoint")->isRequired()->cannotBeEmpty()->end()
                ->scalarNode("version")->isRequired()->cannotBeEmpty()->end()
                ->scalarNode("store_id")->isRequired()->cannotBeEmpty()->end()
                ->arrayNode("store_ids")
                  ->prototype("scalar")->end()
                ->end()
                ->scalarNode("username")->isRequired()->cannotBeEmpty()->end()
                ->scalarNode("password")->isRequired()->cannotBeEmpty()->end()
              ->end()
            ->end()
          ->end()
        ->end()
      ->end()
      ;

    return $treeBuilder;
  }
}
