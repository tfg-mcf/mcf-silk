<?php

namespace Silk\Config\Trees;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class CronConfig implements ConfigurationInterface
{
  public function getConfigTreeBuilder ()
  {
    $treeBuilder = new TreeBuilder;

    $rootNode = $treeBuilder->root("cron");

    $rootNode
      ->children()
        ->arrayNode("cronitor")
          ->children()
            ->scalarNode("base_url")->defaultValue("https://cronitor.link")->cannotBeEmpty()->end()
            ->scalarNode("auth_key")->isRequired()->cannotBeEmpty()->end()
            ->arrayNode("monitors")
              ->prototype("scalar")->end()
            ->end()
        ->end()
      ->end()
      ;

    return $treeBuilder;
  }
}
