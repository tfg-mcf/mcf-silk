<?php

namespace Silk\Config;

use Silk\Config\Loaders\PhpLoader;
use Symfony\Component\Config\Definition\Processor as SymfonyProcessor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Finder\Finder;

class Processor extends SymfonyProcessor
{
  protected $directories;

  protected $finder;

  protected $locator;

  public function __construct (array $directories = array())
  {
    $this->directories = $directories;
    $this->finder = new Finder;
    $this->locator = new FileLocator($this->directories);
  }

  public function processConfig ()
  {
    return $this->flattenConfigs($this->directories);
  }

  protected function findConfigs ()
  {
    $configFiles = array();
    foreach ($this->finder->files()->in($this->directories)->name("*.php") as $file):
      array_push($configFiles, $file->getPathName());
    endforeach;
    return $configFiles;
  }

  protected function loadConfigs (array $directories = array())
  {
    $configLoaderResolver = new LoaderResolver(array(new PhpLoader($this->locator)));
    $configDelegatingLoader = new DelegatingLoader($configLoaderResolver);

    return $processedConfigs = array_map(function ($configFile)
      use ($configDelegatingLoader) {
        $locatedConfigFile = $this->locator->locate($configFile);
        $loadedConfigFiles = array($configDelegatingLoader->load($locatedConfigFile));
        $configurationClass = "Silk\\Config\\Trees\\".ucwords(pathinfo($locatedConfigFile, PATHINFO_FILENAME))."Config";

        return $this->processConfiguration(
          new $configurationClass,
          $loadedConfigFiles
        );
      },
      $this->findConfigs()
    );
  }

  protected function flattenConfigs ()
  {
    $mergedConfiguration = array();
    foreach ($this->loadConfigs($this->directories) as $config):
      $mergedConfiguration = array_merge($mergedConfiguration, $config);
    endforeach;

    return $mergedConfiguration;
  }
}
