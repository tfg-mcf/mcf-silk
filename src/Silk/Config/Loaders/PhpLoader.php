<?php

namespace Silk\Config\Loaders;

use Symfony\Component\Config\Loader\FileLoader;

class PhpLoader extends FileLoader
{
  public function load ($resource, $type = null)
  {
    return require_once $resource;
  }

  public function supports ($resource, $type = null)
  {
    return is_string($resource) && "php" === pathinfo($resource, PATHINFO_EXTENSION);
  }
}
