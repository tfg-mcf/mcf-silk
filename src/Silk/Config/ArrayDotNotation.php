<?php

namespace Silk\Config;

class ArrayDotNotation
{
  /**
   * Get an array value using dot notation
   *
   * Taken from
   */
  public static function get (array $array = array(), $path, $default = null)
  {
    if (is_array($array) and isset($array[$path]))
      return $array[$path];

    foreach (explode(".", $path) as $segment):
      if (is_object($array)):
        if (isset($array->{$segment})):
          $array = $array->{$segment};
        else:
          return $default and $default instanceof \Closure ?
            $default() : $default;
        endif;
      else:
        if (isset($array[$segment])):
          $array = $array[$segment];
        else:
          return $default and $default instanceof \Closure ?
            $default() : $default;
        endif;
      endif;
    endforeach;

    return $array;
  }

  /**
   * Set an array value using dot notation
   *
   * NOTE: Taken from an answer on StackOverflow.
   * @link https://stackoverflow.com/a/7851701
   */
  public static function push (array &$array = array(), $path, $value)
  {
    $location = &$array;

    foreach (explode(".", $path) as $segment):
      $location = &$location[$segment];
    endforeach;

    return $location = $value;
  }

  /**
   * Unset an array using dot notation
   *
   * Could probably be improved/optimized.
   * NOTE: Taken from a blog post by Christopher Pitt (@assertchris)
   * @link https://medium.com/@assertchris/dot-notation-3fd3e42edc61
   */
  public static function pop (array &$array = array(), $path)
  {
    $segments = explode(".", $path);

    while (count($segments) > 1):
      $segment = array_shift($segments);

      if (isset($array[$segment]) and is_array($array[$segment]))
        $array =& $array[$segment];
    endwhile;

    unset($array[array_shift($segments)]);
  }

  /**
   * Check if an element exists in an array using dot notation
   *
   * NOTE: Taken from a blog post by Christopher Pitt (@assertchris)
   * @link https://medium.com/@assertchris/dot-notation-3fd3e42edc61
   */
  public static function exists (array $array = array(), $path)
  {
    if (isset($array[$path]))
      return true;

    foreach (explode(".", $path) as $segment):
      if (!is_array($array) or !isset($array[$segment]))
        return false;

      $array = $array[$segment];
    endforeach;

    return true;
  }
}
