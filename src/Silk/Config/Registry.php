<?php

namespace Silk\Config;

class Registry
{
  /**
   * @property object $instance */
  private static $instance;

  /**
   * @property array $store Array containing objects, classes, variables, and other data */
  private $store;

  /**
   * Constructor method
   * @method __construct
   */
  protected function __construct (array $data = array())
  {
    $this->store = $data;
  }

  /**
   * Static instance creation
   * @method __init
   * @return self
   */
  public static function __init (array $data = array())
  {
    if ( self::$instance == null ) {
      self::$instance = new self($data);
    }
    return self::$instance;
  }

  /**
   * Static setter method
   * @method add
   * @param string $key The name of the index being set in the registry.
   * @param mixed $value The value being assigned to the index ($key)
   */
  public static function add ( $key , $value )
  {
    $instance = self::__init ();
    // $instance->store [ $key ] = $value; # NOTE: default class setter method
    ArrayDotNotation::push($instance->store, $key, $value);
  }

  /**
   * Static getter method
   * @method load
   * @param string $key Name of the index being retrieved from the registry.
   * @return mixed
   */
  public static function load ( $key, $default = null )
  {
    $instance = self::__init ();
    // return $instance->store [ $key ]; # NOTE: default class getter method
    return ArrayDotNotation::get($instance->store, $key, $default);
  }

  /**
   * Static property check method
   * @method stored
   * @param string $key Name of the index being checked
   * @return boolean
   */
  public static function stored ( $key )
  {
    $instance = self::__init ();
    // return isset ( $instance->store [ $key ] ); # NOTE: default class isser method
    return ArrayDotNotation::exists($instance->store, $key);
  }

  /**
   * Static unset property method
   * @method remove
   * @param string $key Name of the index being removed
   */
  public static function remove ( $key )
  {
    $instance = self::__init ();
    // unset ( $instance->store [ $key ] ); # NOTE: default class unsetter method
    ArrayDotNotation::pop($instance->store, $key);
  }

  /**
   * Get a nicely formatted output of objects currently in the registry
   * @method output
   */
  public static function output ()
  {
    $instance = self::__init ();
    return get_object_vars ( $instance );
  }

  /** Sleep method for data serialization */
  private function __sleep ()
  {
    $this->store = serialize ( $this->store );
  }

  /** Wake method for unserialization of the data */
  private function __wakeup ()
  {
    $this->store = unserialize ( $this->store );
  }
}
