<?php

namespace Silk;

use Cronitor\Monitor;
use Symfony\Component\Console\Style\SymfonyStyle;

class Utils
{
  /**
   * Deprecate a function or method
   */
  public static function deprecate ($message)
  {
    $backtrace = debug_backtrace();
    $caller = next($backtrace);
    echo PHP_EOL;
    trigger_error("Method call {$caller['class']}::{$caller['function']} in {$caller['file']} on line {$caller['line']}\n{$message}", E_USER_DEPRECATED);
    echo PHP_EOL;
  }

  /**
   * Generates a time stamp from a specified format.
   * @return string
   */
  public static function timestamp
  (string $format='m-d-Y H:i:s')
  {
    $now = new \DateTime();
    return $now->format($format);
  }

  /**
   * Trim right side trailing zeroes
   * @return string
   */
  public static function rtrim_zeroes
  (string $subject)
  {
    return strpos($subject, ".") !== false ? rtrim(rtrim($subject, "0"), ".") : $subject;
  }

  /**
   * Check if a database query has results or not
   * @return boolean
   */
  public static function has_results
  ($query)
  {
    if (is_object($query))
      return $query->rowCount() <= 0 ? false : true;

    if (is_int($query))
      return $query <= 0 ? false : true;
  }

  /**
   * Connect to a MySQL database via PDO
   * @param object $env
   * @param object $monitor
   * @return object
   */
  public static function connect_database
  (array $env, SymfonyStyle $io = null, Monitor $monitor = null)
  {
    try {
      $connection = new \PDO(
        "mysql:host={$env['host']};port={$env['port']};dbname={$env['schema']}",
        $env['username'],
        $env['password']
      );

      if ($io):
        if ($io->isVerbose()) $io->text("Success! {$connection->getAttribute(\PDO::ATTR_CONNECTION_STATUS)}");
      endif;

      return $connection;
    }
    catch (\PDOException $e) {
      if ($monitor) $monitor->fail("{$e->getMessage()}");
      throw new \PDOException(" {$e->getMessage()}".PHP_EOL);
    }
  }

  /**
   * Build a set of media gallery entries for a Magento product
   * @return array
   */
  public static function get_media_gallery_entries
  (array $images, string $format='jpeg')
  {
    $counter = 0;
    $entries = [];

    foreach ($images as $image):
      $counter++;
      $entry = [
        'position' => $counter,
        'label' => '',
        'media_type' => 'image',
        'disabled' => false,
        'types' => ($counter == 1 ? ['image', 'small_image', 'thumbnail', 'swatch_image'] : []),
        'content' => [
          'type' => "image/{$format}",
          'name' => pathinfo($image, PATHINFO_FILENAME).'.'.pathinfo($image, PATHINFO_EXTENSION),
          'base64_encoded_data' => base64_encode(file_get_contents($image)),
        ],
      ];
      array_push($entries, $entry);
    endforeach;

    return $entries;
  }

  /**
   * @deprecated
   *
   * Perform conversion on Silk pricing and stock levels for Magento
   *
   * @return mixed
   */
  public static function silk_math
  ($value, string $operator, $unit)
  {
    self::deprecate("Use Silk\\Math and it's operational functions instead");

    switch ($unit):
      case '/C': { // per 100
        switch ($operator):
          case '*':
            return number_format(($value * 100), 2, '.', '');
            break;
          case '/':
            return number_format(($value / 100), 2, '.', '');
            break;
        endswitch;
      }
      case '/M': { // per 1,000
        switch ($operator):
          case '*':
            return number_format(($value * 1000), 2, '.', '');
            break;
          case '/':
            return number_format(($value / 1000), 2, '.', '');
            break;
        endswitch;
      }
      default: // EA, BX, CS, PR,
        return $value;
    endswitch;
  }

  /**
   * Check if a stock level is in stock or not
   * @return boolean
   */
  public static function is_in_stock
  ($qty)
  {
    return $qty > 0 ? true : false;
  }
}
