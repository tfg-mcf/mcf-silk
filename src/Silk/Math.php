<?php

namespace Silk;

class Math
{
  /**
   * @method Multiply
   *
   * Multiply two numbers from the Silk database together
   * and return the result as a human readable integer
   *
   * @param int $a The first item in the equation (left of the operator)
   * @param int $b The second item in the equation (right of the operator)
   * @return int
   */
  public static function multiply ($a, $b)
  {
    return number_format(($a * $b), 2, ".", "");
  }

  /**
   * @method Divide
   *
   * Divide two numbers from the Silk database together
   * and return the result as a human readable integer
   *
   * @param int $a The first item in the equation (left of the operator)
   * @param int $b The second item in the equation (right of the operator)
   * @return int
   */
  public static function divide ($a, $b)
  {
    return number_format(($a / $b), 2, ".", "");
  }
}
