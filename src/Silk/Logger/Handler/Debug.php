<?php

namespace Silk\Logger\Handler;

use Monolog\Logger;

class Debug extends AbstractHandler
{
  protected $fileName = "debug.log";

  protected $logLevel = Logger::DEBUG;
}
