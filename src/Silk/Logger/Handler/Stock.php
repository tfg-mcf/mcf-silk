<?php

namespace Silk\Logger\Handler;

use Monolog\Logger;

class Stock extends AbstractHandler
{
  protected $fileName = "stock.log";

  protected $logLevel = Logger::DEBUG;
}
