<?php

namespace Silk\Logger\Handler;

use Monolog\Logger;

class Info extends AbstractHandler
{
  protected $fileName = "info.log";

  protected $logLevel = Logger::INFO;
}
