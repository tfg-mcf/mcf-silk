<?php

namespace Silk\Logger\Handler;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Silk\Config\Registry;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

abstract class AbstractHandler extends StreamHandler
{
  protected $filesystem;

  protected $fileName;

  protected $logLevel = Logger::DEBUG;

  public function __construct (Filesystem $filesystem, string $filePath)
  {
    $this->filesystem = $filesystem;

    parent::__construct("{$filePath}/{$this->fileName}", $this->logLevel);
  }

  public function write (array $record)
  {
    if (!$this->filesystem->exists($this->url))
      $this->filesystem->mkdir(Registry::load("log_directory"));

    parent::write($record);
  }
}
