<?php

namespace Silk\Logger\Handler;

use Monolog\Logger;

class Error extends AbstractHandler
{
  protected $fileName = "error.log";

  protected $logLevel = Logger::ERROR;
}
