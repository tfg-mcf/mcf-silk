<?php

// Configuration array
return (object) [
  'silk' => (object) [
    'host' => '',
    'port' => 0,
    'schema' => '',
    'username' => '',
    'password' => '',
  ],
  'mage' => (object) [
    'host' => '',
    'port' => 0,
    'schema' => '',
    'username' => '',
    'password' => '',
  ],
  'api' => (object) [
    'version' => '',
    'base_uri' => '',
    'endpoint' => '',
    'store_id' => '',
    'username' => '',
    'password' => '',
  ],
  'cronitor' => (object) [
    'base_url' => 'https://cronitor.link',
    'auth_key' => '',
    'monitors' => array (),
  ],
];
