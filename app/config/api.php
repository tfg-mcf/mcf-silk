<?php

return array (
  "clients" => array (

    # Magento 2
    "mage" => array (
      "base_uri" => getenv("MAGE__API__BASE_URI"),
      "endpoint" => "index.php/rest",
      "version" => "V1",
      "store_id" => "all",
      "store_ids" => array (
        "all",
        "default",
        "en",
      ),
      "username" => getenv("MAGE__API__USERNAME"),
      "password" => getenv("MAGE__API__PASSWORD"),
    ),

  ),
);
