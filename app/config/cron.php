<?php

return array (
  "cronitor" => array (

    "base_url" => "https://cronitor.link",
    "auth_key" => getenv("CRONITOR__AUTH_KEY"),
    "monitors" => array (
      "silk_heartbeat_inventory_sync" => getenv("CRONITOR__MONITORS__SILK_HEARTBEAT_INVENTORY_SYNC"),
    ),

  ),
);
