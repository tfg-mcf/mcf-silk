<?php

return array (
  "connections" => array (

    "silk" => array (
      "host" => getenv("SILK__DATABASE__HOST"),
      "port" => (int) getenv("SILK__DATABASE__PORT"),
      "schema" => getenv("SILK__DATABASE__SCHEMA"),
      "username" => getenv("SILK__DATABASE__USERNAME"),
      "password" => getenv("SILK__DATABASE__PASSWORD"),
    ),

    "mage" => array (
      "host" => getenv("MAGE__DATABASE__HOST"),
      "port" => (int) getenv("MAGE__DATABASE__PORT"),
      "schema" => getenv("MAGE__DATABASE__SCHEMA"),
      "username" => getenv("MAGE__DATABASE__USERNAME"),
      "password" => getenv("MAGE__DATABASE__PASSWORD"),
    ),

  ),
);
