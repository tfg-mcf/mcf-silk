<?php

# Define global application constants available for use
define("ROOT_DIR", realpath(dirname(__FILE__)."/../"));
define("APPLICATION_VERSION", json_decode(file_get_contents(ROOT_DIR."/composer.json"))->version);

# Require vendor code autoloader
$autoloader = ROOT_DIR."/vendor/autoload.php";
if (!file_exists($autoloader)):
  throw new Exception("No autoload.php was found in ".ROOT_DIR."/vendor/. Please try a composer install.");
else:
  require_once $autoloader;
endif;

# Import dependencies for our bootstrap code (lol)
use Silk\Config\Processor;
use Silk\Config\Registry;
use Silk\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Dotenv\Dotenv;

# Load environment file(s) - add more by adding another
# argument to the Dotenv::load method call below
# NOTE: Access environment variables via getenv() please.
$env = new Dotenv;
$env->load(
  ROOT_DIR."/app/etc/.env"
);

# Process an array of directories that the application
# configuration files should be searched for in
$processor = new Processor(array(
  ROOT_DIR."/app/config",
));

# Initialize the application registry and seed it with
# the processed configuaration data
Registry::__init($processor->processConfig());

# First, load the application service container
# Next, store the service container in the application registry
$container = new ContainerBuilder;
$extension = new Extension;
$extension->load(array(), $container);
Registry::add("services", $container);

# Store the applications' commands in the application registry
Registry::add("commands", array_map(function ($file) {
  $command = "Silk\\Commands\\".pathinfo($file, PATHINFO_FILENAME);
  return new $command;
}, glob(ROOT_DIR."/src/Silk/Commands/*.php")));
