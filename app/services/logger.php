<?php

use Silk\Config\Registry;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

# Application Logger
$container->setAlias("Logger", "Silk\Logger\Logger");
$container
  ->register("Silk\Logger\Logger", "\\Silk\\Logger\\Logger")
  ->addArgument("Anzio Silk")
  ;

# Stock Level StreamHandler
$container->setAlias("StreamHandler\Stock", "Silk\Logger\Handler\Stock");
$container
  ->register("Silk\Logger\Handler\Stock", "\\Silk\\Logger\\Handler\\Stock")
  ->addArgument(new Reference("Filesystem"))
  ->addArgument(Registry::load("log_directory"))
  ;

# Debug StreamHandler
$container->setAlias("StreamHandler\Debug", "Silk\Logger\Handler\Debug");
$container
  ->register("Silk\Logger\Handler\Debug", "\\Silk\\Logger\\Handler\\Debug")
  ->addArgument(new Reference("Filesystem"))
  ->addArgument(Registry::load("log_directory"))
  ;

# Info StreamHandler
$container->setAlias("StreamHandler\Info", "Silk\Logger\Handler\Info");
$container
  ->register("Silk\Logger\Handler\Info", "\\Silk\\Logger\\Handler\\Info")
  ->addArgument(new Reference("Filesystem"))
  ->addArgument(Registry::load("log_directory"))
  ;

# Error StreamHandler
$container->setAlias("StreamHandler\Error", "Silk\Logger\Handler\Error");
$container
  ->register("Silk\Logger\Handler\Error", "\\Silk\\Logger\\Handler\\Error")
  ->addArgument(new Reference("Filesystem"))
  ->addArgument(Registry::load("log_directory"))
  ;
