<?php

use Silk\Config\Registry;
use Symfony\Component\DependencyInjection\{Parameter, Referene};

$container
  ->register("Mage\Api\Client", "\\Mage\\Api\\Client")
  ->addArgument(Registry::load("clients.mage"))
  ;
