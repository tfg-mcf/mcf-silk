<?php

use Silk\Config\Registry;
use Symfony\Component\DependencyInjection\{Parameter, Reference};

$container->setAlias("HeartbeatMonitor\Inventory", "Silk\Monitor\Heartbeat\InventorySync");
$container
  ->register("Silk\Monitor\Heartbeat\InventorySync", "\\Cronitor\\Monitor")
  ->addArgument(Registry::load("cronitor.monitors.silk_heartbeat_inventory_sync"))
  ->addArgument(array(
    "auth_key" => Registry::load("cronitor.auth_key"),
    "base_url" => Registry::load("cronitor.base_url"),
  ))
  ;
