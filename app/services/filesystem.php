<?php

use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

$container->setAlias("Filesystem", "Symfony\Component\Filesystem\Filesystem");
$container
  ->register("Symfony\Component\Filesystem\Filesystem", "\\Symfony\\Component\\Filesystem\\Filesystem")
  ;
