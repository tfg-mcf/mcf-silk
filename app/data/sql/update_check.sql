--
-- Magento query - price update check
--

SELECT
inv_ent.entity_id AS id,
inv_ent.sku,
inv_dec.value AS price,
inv_stock.qty,
inv_ent.type_id AS type,
inv_int.value AS status,
inv_ent.updated_at
FROM
catalog_product_entity AS inv_ent,
catalog_product_entity_decimal AS inv_dec,
cataloginventory_stock_item AS inv_stock,
catalog_product_entity_int AS inv_int
WHERE inv_dec.attribute_id = 74 -- NOTE *
AND inv_int.attribute_id = 94 -- NOTE **
AND inv_ent.type_id NOT LIKE "configurable"
AND inv_ent.entity_id = inv_stock.product_id
AND inv_ent.entity_id = inv_dec.entity_id
AND inv_ent.entity_id = inv_int.entity_id
AND inv_int.entity_id = :id
GROUP BY
inv_ent.entity_id,
inv_stock.item_id,
inv_dec.value,
inv_int.value
ORDER BY inv_ent.entity_id
;

-- NOTE *: (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'price')
-- NOTE **: (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'status')
