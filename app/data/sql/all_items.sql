--
-- Silk query - get all items
--

SELECT
i12.___recno AS id,
i12.itemcode AS sku,
i2.description AS name,
IF ((i12.quantityonhand - i12.quantityreserved) < 0, 0, (i12.quantityonhand - i12.quantityreserved)) AS qty,
i2.`regularprice(1stu_m)` AS price,
i2.`pricea(1stu_m)` AS price_a,
i2.`priceb(1stu_m)` AS price_b,
i2.`pricec(1stu_m)` AS price_c,
i2.`priced(1stu_m)` AS price_d,
i2.`pricee(1stu_m)` AS price_e,
i2.1stu_m AS u_m,
i2.itemcategory AS silk_cat,
i2.itemdivision AS silk_div
FROM invf02 AS i2
INNER JOIN invf12 AS i12
ON i12.itemcode = i2.itemcode
WHERE i2.itemtype = 'I'
AND i2.itemcategory LIKE :cat -- Category
AND i2.itemdivision LIKE :div -- Division
AND i2.itemcode LIKE :sku -- SKU
AND i12.locationcode = 70
AND i12.locationkey = 00
AND (
	quantityonhand > 0
	OR qtyonorder > 0
	OR lastsalesdate > '2016-01-01'
)
ORDER BY i12.___recno
;
