--
-- Silk query - get updated pricing levels
-- SELECT * FROM vw_invf02 ORDER BY itemcode;
--

SELECT
i12.___recno AS id,
i2.___timestampupdated AS updated_at,
i2.itemcode AS sku,
i2.`1stu_m` AS u_m,
i2.`lastcost(1stu_m)` AS cost,
i2.`regularprice(1stu_m)` AS price,
i2.`pricea(1stu_m)` AS price_a,
i2.`priceb(1stu_m)` AS price_b,
i2.`pricec(1stu_m)` AS price_c,
i2.`priced(1stu_m)` AS price_d,
i2.`pricee(1stu_m)` AS price_e
FROM vw_invf02 AS i2
LEFT JOIN invf12 AS i12
ON i2.itemcode = i12.itemcode
GROUP BY i2.itemcode
ORDER BY i2.itemcode
;
