--
-- Magento query - search by SKU
--

SELECT
inv_ent.entity_id AS id,
inv_ent.updated_at,
inv_ent.sku,
inv_var.value AS name,
inv_int.value AS status
FROM
catalog_product_entity AS inv_ent,
catalog_product_entity_varchar AS inv_var,
catalog_product_entity_int AS inv_int
WHERE inv_var.attribute_id = 70 -- NOTE *
AND inv_int.attribute_id = 94 -- NOTE **
AND inv_ent.entity_id = inv_var.entity_id
AND inv_ent.entity_id = inv_int.entity_id
AND inv_ent.sku LIKE :sku  -- Product SKU
AND inv_var.value LIKE :name -- Product name
AND inv_var.value NOT LIKE :exclude -- Exclude by product name
AND inv_int.value LIKE 2 -- Disabled products
GROUP BY
inv_ent.entity_id,
inv_var.value,
inv_int.value
ORDER BY inv_ent.entity_id
;

-- NOTE *: (SELECT inv_var.attribute_id FROM eav_attribute WHERE inv_var.store_id = 0 AND entity_type_id = 4 AND attribute_code = 'name')
-- NOTE **: (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'status')
