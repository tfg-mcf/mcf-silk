--
-- Silk query - get updated quantity levels
-- SELECT * FROM vw_invf12 ORDER BY itemcode;
--

SELECT
i12.___recno AS id,
i12.___timestampupdated AS updated_at,
i12.itemcode AS sku,
IF((i12.quantityonhand - i12.quantityreserved) < 0, 0, (i12.quantityonhand - i12.quantityreserved)) AS qty,
i2.1stu_m AS u_m
FROM vw_invf12 AS i12
LEFT JOIN invf02 AS i2
ON i12.itemcode = i2.itemcode
ORDER BY i12.itemcode
;
